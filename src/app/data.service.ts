import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  private baseUrl = environment.baseUrl;

  constructor(private http :HttpClient) { }

  getAgencies():Observable<any>{
    console.log("supplyform.ts");
    return this.http.get(this.baseUrl+'agencyNames');
  } 


  getSuppliersList():Observable<any>{
    console.log("supplyform.ts");
    return this.http.get(this.baseUrl+'supplierList');
  }

  getSiteNameList():Observable<any>{
    console.log("supplyform.ts");
    return this.http.get(this.baseUrl+'siteList');
  }

  getTransporterList():Observable<any>{
    console.log("transporterList");
    return this.http.get(this.baseUrl+'transporterList');
  }

  getSitenames():Observable<any>{
    console.log("siteList");
    return this.http.get(this.baseUrl+'siteList');
  }

  getItemOfWorkList():Observable<any>{
    console.log("itemOfWork");
    return this.http.get(this.baseUrl+'itemOfWork');
  }

  getAccountHolderList():Observable<any>{
    console.log("accountHolderList");
    return this.http.get(this.baseUrl+'accountHolderList');
  }

  getMaterialList():Observable<any>{
    console.log("materialList");
    return this.http.get(this.baseUrl+'materialList');
  }

  getMaterialCost(matId,supId,unitId):Observable<any>{
    console.log("mtr id :"+ matId +" : supid : "+supId);
    return this.http.get(this.baseUrl+'materialRate?suppId='+supId+'&matrId='+matId+'&unitId='+unitId);
  }

  getAgencyDetails(agId):Observable<any>{
    console.log("agId id :"+ agId);
    return this.http.get(this.baseUrl+'agencyDetails?agencyId='+agId);
  }

  getUnits():Observable<any>{
    console.log("getUnits");
    return this.http.get(this.baseUrl+'getUnits');
  }

  getDistance():Observable<any>{
    console.log("supplyform.ts");
    return this.http.get(this.baseUrl+'getDistanceTypes');
  }

  getVehicleType():Observable<any>{
    console.log("getVehicleTypes");
    return this.http.get(this.baseUrl+'getVehicleTypes');
  }

  getAgencyNameList():Observable<any>{
    console.log("supplyform.ts");
    return this.http.get(this.baseUrl+'agencyNames');
  }

  getModeOfPaymentList():Observable<any>{
    console.log("financeform.ts");
    return this.http.get(this.baseUrl+'paymentTypes');
  }

  getTypeOfWorkList():Observable<any>{
    console.log("financeform.ts");
    return this.http.get(this.baseUrl+'typeOfWork');
  }

  getSuppliersListM():Observable<any>{
    console.log("adminform.ts");
    return this.http.get(this.baseUrl+'supplierList');
  }

  saveSuppyData(objSupply):Observable<any>{
    console.log("save.ts **  " + JSON.stringify(objSupply));
    return this.http.post(this.baseUrl+'saveSupplyData',objSupply);
  }

  saveFinData(objFin):Observable<any>{
    console.log("save.ts +" );
    return this.http.post(this.baseUrl+'saveFinanceData',objFin);
  }

  saveAgencyData(objAgency):Observable<any>{
    console.log("save agency +" );
    return this.http.post(this.baseUrl+'addOrEditAgency',objAgency);
  }

  getAccountHoldersReportData():Observable<any>{
    console.log("getAccountHoldersReportData.ts");
    return this.http.get(this.baseUrl+'accountholdersreport');
  }

  getTransportData():Observable<any>{
    console.log("getTransportData.ts");
    return this.http.get(this.baseUrl+'transportDataReport');
  }

  getTotalSummary():Observable<any>{
    console.log("getTotalSummary");
    return this.http.get(this.baseUrl+'totalSummaryReport');
  }
  
  saveNewSite(objAgency):Observable<any>{
    console.log("saveNewSite +" );
    return this.http.post(this.baseUrl+'addorEditSite',objAgency);
  }

  getSiteDetails(siteId):Observable<any>{
    console.log("siteId id :"+ siteId);
    return this.http.get(this.baseUrl+'siteDetails?siteId='+siteId);
  }

  updateSite(objSite):Observable<any>{
    console.log("updateSite +" );
    return this.http.post(this.baseUrl+'updateSite',objSite);
  }

  addMaterial(objSite):Observable<any>{
    console.log("addMaterial +" );
    return this.http.post(this.baseUrl+'addMaterial',objSite);
  }

  updateMaterialRate(obj):Observable<any>{
    console.log("updateMaterialRate +" );
    return this.http.post(this.baseUrl+'updateMaterial',obj);
  }

  loginCheck(objSite):Observable<any>{
    console.log("LOGIN +" );
    return this.http.post(this.baseUrl+'checkUser',objSite);
  }

  addVehicle(objVehicle):Observable<any>{
    console.log("addVehicle +" );
    return this.http.post(this.baseUrl+'addVehicle',objVehicle);
  }

  addMiscExpense(objMisc):Observable<any>{
    console.log("addMiscExpense +" );
    return this.http.post(this.baseUrl+'miscExpense',objMisc);
  }

  getSupplierDetails(sId):Observable<any>{
    console.log("supplierDetails id :"+ sId);
    return this.http.get(this.baseUrl+'supplierDetails?acctid='+sId);
  }

  getTransporterDetails(tId):Observable<any>{
    console.log("getTransporterDetails id :"+ tId);
    return this.http.get(this.baseUrl+'transporterDetails?transporterid='+tId);
  }

  getFuelDetails(accId):Observable<any>{
    console.log("getFuelDetails id :"+ accId);
    return this.http.get(this.baseUrl+'fuelDetails?accid='+accId);
  }

  getSiteSummary():Observable<any>{
    console.log("getSiteSummary");
    return this.http.get(this.baseUrl+'siteReport');
  }

  getSiteSupplyDetails(siteid):Observable<any>{
    console.log("getSiteSupplyDetails id :"+ siteid);
    return this.http.get(this.baseUrl+'siteSupplierDetails?siteid='+siteid);
  }

  getSiteTransportDetails(siteid):Observable<any>{
    console.log("siteTransportDetails id :"+ siteid);
    return this.http.get(this.baseUrl+'siteTransportDetails?siteid='+siteid);
  }

  getSiteMiscDetails(siteid):Observable<any>{
    console.log("siteMiscDetails id :"+ siteid);
    return this.http.get(this.baseUrl+'siteMiscDetails?siteid='+siteid);
  }
  
  getFuelTypes():Observable<any>{
    console.log("getFuelTypes");
    return this.http.get(this.baseUrl+'fuelTypes');
  }

  getDiesalSuppliers():Observable<any>{
    console.log("getDiesalSuppliers");
    return this.http.get(this.baseUrl+'diesalSupplierList');
  }

  saveFuelPurchased(objFuelP):Observable<any>{
    console.log("saveFuelPurchased.ts **  " + JSON.stringify(objFuelP));
    return this.http.post(this.baseUrl+'saveFuelPurchased',objFuelP);
  }

  saveFuelDistribution(objFuelD):Observable<any>{
    console.log("saveFuelDistribution.ts **  " + JSON.stringify(objFuelD));
    return this.http.post(this.baseUrl+'saveFuelDistribution',objFuelD);
  }

  getAvailableFuelQty(fuelTypeid):Observable<any>{
    console.log("getAvailableFuelQty id :"+ fuelTypeid);
    return this.http.get(this.baseUrl+'getFuelAvailable?fuelId='+fuelTypeid);
  }

  getFuelSummary():Observable<any>{
    console.log("getFuelSummary");
    return this.http.get(this.baseUrl+'fuelSummary');
  }

  saveMachineryData(objMach):Observable<any>{
    console.log("save.ts **  " + JSON.stringify(objMach));
    return this.http.post(this.baseUrl+'saveMachineryData',objMach);
  }

  getMacineryReport():Observable<any>{
    console.log("getMacineryReport");
    return this.http.get(this.baseUrl+'MacineryReport');
  }

  getSiteMachineDetails(siteid):Observable<any>{
    console.log("getSiteMachineDetails id :"+ siteid);
    return this.http.get(this.baseUrl+'siteMachineryDetails?siteid='+siteid);
  }

  getMops():Observable<any>{
    console.log("getMOPtypes");
    return this.http.get(this.baseUrl+'getMOPtypes');
  }

  getFinanceReport():Observable<any>{
    console.log("financeReport");
    return this.http.get(this.baseUrl+'financeReport');
  }

  getFinanceDetails(accId):Observable<any>{
    console.log("getFinanceDetails id :"+ accId);
    return this.http.get(this.baseUrl+'financeData4Summary?accholder='+accId);
  }

  deleteMachinery(obj):Observable<any>{
    console.log("deleteMacinery +" );
    return this.http.post(this.baseUrl+'deleteMacinery',obj);
  }

  deleteMisc(obj):Observable<any>{
    console.log("deleteMisc +" );
    return this.http.post(this.baseUrl+'deleteMisc',obj);
  }

  detailsToDelete(obj):Observable<any>{
    console.log("detailsToDelete id :"+ obj);
    return this.http.post(this.baseUrl+'detailsToDelete',obj);
  }

  updateMiscExpense(objMisc):Observable<any>{
    console.log("updateMiscExpense +" );
    return this.http.post(this.baseUrl+'updateMiscExpense',objMisc);
  }

}
