import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  FirstTypeWork = 'Road Constructions';
  roadPara = ' Road construction facilities include: Valuation and development of asphalt properties.Determination of asphalt component quality and suitability. Development of new road products, including roll pave and road materials.';
  
  username : any = "";
  showlinks : any = true;

  constructor() {
    this.showlinks = true;
   }
 
  ngOnInit() {

    this.username = localStorage.getItem('username');
    console.log("home : " +localStorage.getItem('username'));
    if(this.username != null){
      this.showlinks = true;
    }
    console.log("home  showlinks : " +localStorage.getItem('username') + " "+ this.showlinks);
  }
  
  onSubmit(){
    
  }
}
