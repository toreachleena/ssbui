import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransporterGridRptComponent } from './transporter-grid-rpt.component';

describe('TransporterGridRptComponent', () => {
  let component: TransporterGridRptComponent;
  let fixture: ComponentFixture<TransporterGridRptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransporterGridRptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransporterGridRptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
