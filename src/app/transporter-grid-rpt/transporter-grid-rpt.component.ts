import { Component, OnInit } from '@angular/core';


import {  
  ColDef,  
  GridApi,  
  ColumnApi  
} from 'ag-grid-community'; 
import { DataService } from '../data.service';
import { ButtonRendererComponent } from '../renderer/button-renderer.component';


@Component({
  selector: 'app-transporter-grid-rpt',
  templateUrl: './transporter-grid-rpt.component.html',
  styleUrls: ['./transporter-grid-rpt.component.css']
})
export class TransporterGridRptComponent implements OnInit {

  // row data and column definitions  
  public summaryData: [];  
  public suppData: [];
  public transportData: [];
  public fuelData: [];
  public financeData: [];
  public heading:any;
  
 // @Input('summaryData') summaryDataC: [];
  public columnDefs: ColDef[];  
  frameworkComponents;
  rowDataClicked1 = {};
  public show:boolean = false;
  public show1:boolean = false;
  public show2:boolean = false;
  public show3:boolean = false;

  // gridApi and columnApi  
  private api: GridApi;  
  private columnApi: ColumnApi;  
  public columnDefsS: ColDef[];

  // gridApi and columnApi  
  private apiS: GridApi;  
  private columnApiS: ColumnApi; 

  private apiT: GridApi;  
  private columnApiT: ColumnApi;
  public columnDefsT: ColDef[];

  public columnDefsF: ColDef[];
  private columnApiF: ColumnApi;
  private apiF: GridApi;  

  public columnDefsFin: ColDef[];
  private columnApiFin: ColumnApi;
  private apiFin: GridApi;  
 

  constructor(private dataService: DataService) {  

    this.frameworkComponents = {
      buttonRenderer: ButtonRendererComponent,
    }

    this.columnDefs = this.createColumnDefs();  
    this.columnDefsS = this.createColumnDefsS(); 
    this.columnDefsT = this.createColumnDefsT(); 
    this.columnDefsF = this.createColumnDefsF(); 
    this.columnDefsFin = this.createColumnDefsFin();

} 

onGridReady(params): void {  
  this.api = params.api;  
  this.columnApi = params.columnApi;  
  this.api.sizeColumnsToFit();  

  
} 

onGridReady1(params) {
  this.apiS = params.api;  
  this.columnApiS = params.columnApi;  
  this.apiS.sizeColumnsToFit(); 
}

onGridReady2(params) {
  this.apiT = params.api;  
  this.columnApiT = params.columnApi;  
  this.apiT.sizeColumnsToFit(); 
}

onGridReady3(params) {
  this.apiF = params.api;  
  this.columnApiF = params.columnApi;  
  this.apiF.sizeColumnsToFit(); 
}

onGridReady4(params) {
  this.apiFin = params.api;  
  this.columnApiFin = params.columnApi;  
  this.apiFin.sizeColumnsToFit(); 
}

  ngOnInit() {
    this.dataService.getTotalSummary().subscribe(data => {  
      this.summaryData = data  ;
  })
  }
  
  private createColumnDefs() {  
    return [ 
      {  
        headerName: 'Account Holder',  
        field: 'agencyName',  
        filter: true,  
       
        resizable:true,
        sortable: true  
    },
    {  
      headerName: 'Supply Cost',  
      field: 'supplyCost',  
      filter: true,  
      
      resizable:true,
      sortable: true  
  },
  {  
    headerName: 'Transport Amount',  
    field: 'transportCost',  
    filter: true,  
    editable: false,  
    resizable:true,
    sortable: true  
},
{  
  headerName: 'Fuel Cost',  
  field: 'fuelCost',  
  filter: true,  
  editable: false,  
  resizable:true,
  sortable: true  
},
{  
  headerName: 'AmountPaid',  
  field: 'financeAmount',  
  filter: true,  
  editable: false,  
  resizable:true,
  sortable: true  
},
{  
  headerName: 'Total Amount',  
  field: 'totalCost',  
  filter: true,  
  editable: false,  
  resizable:true,
  sortable: true  
},
{  
 headerName: 'SupplierDetails',
    resizable:true,
      cellRenderer: 'buttonRenderer',
      cellRendererParams: {
        onClick: this.onBtnClick1.bind(this),
        label: 'Supplier'
      }
    },
    {  
        headerName: 'TransporterDetails',
        resizable:true,
             cellRenderer: 'buttonRenderer',
             cellRendererParams: {
               onClick: this.onBtnClick2.bind(this),
               label: 'Transporter'
             }
}
,
    {  
        headerName: 'FuelDetails',
        resizable:true,
             cellRenderer: 'buttonRenderer',
             cellRendererParams: {
               onClick: this.onBtnClick3.bind(this),
               label: 'Fuel'
             }
},
{  
    headerName: 'FinanceDetails',
    resizable:true,
         cellRenderer: 'buttonRenderer',
         cellRendererParams: {
           onClick: this.onBtnClick4.bind(this),
           label: 'Finance'
         }
}
    ]
  }

  private createColumnDefsS() {  
    return [ 
      {  
        headerName: 'Date',  
        field: 'transactiondate',  
        filter: true,  
       
        resizable:true,
        sortable: true  
    },
    {  
      headerName: 'Site',  
      field: 'sitename',  
      filter: true,  
      
      resizable:true,
      sortable: true  
  },
  {  
    headerName: 'Supplier',  
    field: 'supplier',  
    filter: true,  
    editable: false,  
    resizable:true,
    sortable: true  
},
{  
  headerName: 'Material',  
  field: 'materialname',  
  filter: true,  
  editable: false,  
  resizable:true,
  sortable: true  
},
{  
  headerName: 'Unit',  
  field: 'unitname',  
  filter: true,  
  editable: false,  
  resizable:true,
  sortable: true  
}
,
{  
  headerName: 'Cost',  
  field: 'rate',  
  filter: true,  
  editable: false,  
  resizable:true,
  sortable: true  
},
{  
  headerName: 'Quantity',  
  field: 'quantity',  
  filter: true,  
  editable: false,  
  resizable:true,
  sortable: true  
},
{  
  headerName: 'TotalCost',  
  field: 'TotalCost',  
  filter: true,  
  editable: false,  
  resizable:true,
  sortable: true  
}
    ]
  }

  ///////////////////////////////
  /*
Abrevation: "Side Shoulder"
Invoicenumber: "MISSING INVOICE NUM"
ItemOfWorkID: 5
NoOfkms: 0
NoOftrips: 47
agencyname: "Rajanna"
distance_type: "Trip"
siteid: 17
sitename: "Somasamudra to Yerringali via veeranjiaya camp"
transactiondate: "2020-06-16"
transporterid: 64
transportingcost: 400
vehicle_type: "Tractor"
vehiclenumber: "2354.0"
  */

  private createColumnDefsT() {  
    return [ 
      {  
        headerName: 'Date',  
        field: 'transactiondate',  
        filter: true,  
       
        resizable:true,
        sortable: true  
    },
    {  
      headerName: 'Site',  
      field: 'sitename',  
      filter: true,  
      
      resizable:true,
      sortable: true  
  },
  {  
    headerName: 'Vehicle',  
    field: 'vehicle_type',  
    filter: true,  
    editable: false,  
    resizable:true,
    sortable: true  
},
{  
  headerName: 'Vehicle#',  
  field: 'vehiclenumber',  
  filter: true,  
  editable: false,  
  resizable:true,
  sortable: true  
},
{  
  headerName: '#Trips',  
  field: 'NoOftrips',  
  filter: true,  
  editable: false,  
  resizable:true,
  sortable: true  
}
,
{  
  headerName: '#Distance',  
  field: 'NoOfkms',  
  filter: true,  
  editable: false,  
  resizable:true,
  sortable: true  
},
{  
  headerName: 'Cost',  
  field: 'transportingcost',  
  filter: true,  
  editable: false,  
  resizable:true,
  sortable: true  
}
    ]
  }

  /////////// column def for fuel//////////////////

 /* 
 "qtyused": 200,
        "fueltype": "Diesel",
        "vehicle_type": "Tipper 6 Wheeler",
        "agencyid": 45,
        "accountholderid": 44,
        "vehiclenumber": "KA340988",
        "useddate": "2021-07-11",
        "rate": 79.7,
        "fueltypeid": 1,
        "accountholder": "Ayappa",
        "id": 5,
        "agencyname": "Ayappa",
        "vehicletypeid": 1
 */

  private createColumnDefsF() {  
    return [ 

      {  
        headerName: 'Date',  
        field: 'useddate',  
        filter: true,  
        editable: false,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'FuelUsed',  
        field: 'fueltype',  
        filter: true,  
        editable: false,  
        resizable:true,
        sortable: true  
      }
      ,
      {  
        headerName: 'Rate',  
        field: 'rate',  
        filter: true,  
        editable: false,  
        resizable:true,
        sortable: true  
      }
      ,
      {  
        headerName: 'Qty',  
        field: 'qtyused',  
        filter: true,  
        editable: false,  
        resizable:true,
        sortable: true  
      },{  
        headerName: 'Sum',  
        field: 'total',  
        filter: true,  
        editable: false,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'VehicleType',  
        field: 'vehicle_type',  
        filter: true,  
        editable: false,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'Vehicle#',  
        field: 'vehiclenumber',  
        filter: true,  
        editable: false,  
        resizable:true,
        sortable: true  
      }

    ]
    }

  ///////////////////////////////\
  /*
"chq_refNo": "",
        "amount": 2500,
        "transactiondate": "2021-07-07",
        "gst": 0,
        "royalty": 0,
        "modeofpayment": 1,
        "actualamountpaid_id": 50,
        "payment_type": "Cash",
        "total": 2500,
        "amountpaidto": "Harsha",
        "permit": 0,
        "agencyname_id": 50,
        "id": 7,
        "bankname": "",
        "agencyname": "Harsha"
  */
  private createColumnDefsFin() {  
    return [ 

      {  
        headerName: 'Date',  
        field: 'transactiondate',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'Agency',  
        field: 'agencyname',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'AmountPaidTo',  
        field: 'amountpaidto',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'PaymentMode',  
        field: 'payment_type',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'Bank',  
        field: 'bankname',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'Cheque',  
        field: 'chq_refNo',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'Amount',  
        field: 'amount',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'GST',  
        field: 'gst',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'Royalty',  
        field: 'royalty',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'Permit',  
        field: 'permit',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'Total',  
        field: 'total',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      }

    ]
  }
  
  //////////
  onBtnClick1(e) {
    
    this.show = true;
    this.show1 = false;
    this.show2 = false;
    this.show3 = false;
    this.rowDataClicked1 = e.rowData;
    
    this.dataService.getSupplierDetails(this.rowDataClicked1['agencyId']).subscribe(data => {  
      this.suppData = data  ;
      
        var temp = ''
        this.suppData.forEach(m => temp = m['accountholder']
        );

      this.heading = "Transactions Details for AccountHolder : "+temp

  })

  }

  onBtnClick2(e) {
    
    //this.show = !this.show;
    this.show1 = true;
    this.show = false;
    this.show2 = false;
    this.show3 = false;
    this.rowDataClicked1 = e.rowData;
  
 this.dataService.getTransporterDetails(this.rowDataClicked1['agencyId']).subscribe(data => {  
  this.transportData = data  ;
  
    var temp = ''
    this.transportData.forEach(m => temp = m['agencyname']
    );

  this.heading = "Transactions Details for Transporter : "+temp
  
})

  }

  onBtnClick3(e) {
    
    //this.show = !this.show;
    this.show1 = false;
    this.show = false;
    this.show2 = true;
    this.show3 = false;
    this.rowDataClicked1 = e.rowData;
  
 this.dataService.getFuelDetails(this.rowDataClicked1['agencyId']).subscribe(data => {  
  this.fuelData = data  ;
  
    var temp = ''
    this.fuelData.forEach(m => temp = m['agencyname']
    );

  this.heading = "Transactions Details for Account Holder : "+temp
  
})

  }

  onBtnClick4(e) {
    
    //this.show = !this.show;
    this.show1 = false;
    this.show = false;
    this.show2 = false;
    this.show3 = true;
    this.rowDataClicked1 = e.rowData;
  
 this.dataService.getFinanceDetails(this.rowDataClicked1['agencyId']).subscribe(data => {  
  this.financeData = data  ;
  
    var temp = ''
    this.financeData.forEach(m => temp = m['amountpaidto']
    );

  this.heading = "Finance details for AccountHolder : "+temp
  
})

  }

}


