import { Component, OnInit } from '@angular/core';


import {  
  ColDef,  
  GridApi,  
  ColumnApi  
} from 'ag-grid-community'; 
import { DataService } from '../data.service';



@Component({
  selector: 'app-accounts-grid-rpt',
  templateUrl: './accounts-grid-rpt.component.html',
  styleUrls: ['./accounts-grid-rpt.component.css']
})
export class AccountsGridRptComponent implements OnInit {

  title = 'Accounts Report';

  // row data and column definitions  
  public ahreportdata: [];  
  public columnDefs: ColDef[];  
  // gridApi and columnApi  
  private api: GridApi;  
  private columnApi: ColumnApi;  

  constructor(private dataService: DataService) {  
    this.columnDefs = this.createColumnDefs();  
} 

onGridReady(params): void {  
  this.api = params.api;  
  this.columnApi = params.columnApi;  
  this.api.sizeColumnsToFit();  
} 

 // create column definitions  
 private createColumnDefs() {  
  return [ 
    {  
      headerName: 'Date',  
      field: 'transactiondate',  
      filter: true,  
      editable: true,  
      resizable:true,
      sortable: true  
    },
    {
      headerName: 'SiteName',  
      field: 'sitename',  
      filter: true,  
      editable: true,  
      resizable:true,
      sortable: true  
    },
    {  
    headerName: 'AccountHolder',
      field: 'accountholder',  
      resizable:true,
      filter: true
      
      //hide: true
  }, 
  
  {  
    headerName: 'Supplier',  
    field: 'supplier',  
    filter: true,  
    editable: true,  
    resizable:true,
    sortable: true  
},
{  
  headerName: 'WorkName',  
  field: 'Abrevation',  
  filter: true,  
  sortable: true,  
  editable: true,  
  resizable:true,
  //cellRenderer: '<a href="edit-user">{{email}}</a>'  
},
  {  
      headerName: 'Material',  
      field: 'materialname',  
      filter: true,  
      sortable: true,  
      editable: true,  
      resizable:true,
     // cellRenderer: '<a href="edit-user">{{email}}</a>'  
  },
  {  
    headerName: 'Rate',  
    field: 'rate',  
    filter: true,  
    editable: true, 
    resizable:true, 
    sortable: true  
},  {  
      headerName: 'Quantity',  
      field: 'quantity',  
      filter: true,  
      editable: true, 
      resizable:true, 
      sortable: true  
  }, 
  {  
  headerName: 'Material Cost',  
  field: 'materialCost',  
  filter: true,  
  editable: true,  
  resizable:true,
  sortable: true 
 
}

 
 
  ]  
}  

  ngOnInit() {
 
    this.dataService.getAccountHoldersReportData().subscribe(data => {  
      this.ahreportdata = data  ;
  })

  }
}