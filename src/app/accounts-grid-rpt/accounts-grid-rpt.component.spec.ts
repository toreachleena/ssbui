import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountsGridRptComponent } from './accounts-grid-rpt.component';

describe('AccountsGridRptComponent', () => {
  let component: AccountsGridRptComponent;
  let fixture: ComponentFixture<AccountsGridRptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountsGridRptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountsGridRptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
