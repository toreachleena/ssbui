import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-contact-create',
  templateUrl: './supplyform.component.html',
  styleUrls: ['./supplyform.component.css']
})
export class ContactCreateComponent implements OnInit {

  searchValue: string = '';
  supplierList = []
  transporterList = []
  siteNames = []
  itemOfWork = []
  accountHolders = []
  materialList = []
  materialCost :any = "";
  materialCost1:any = "";
  units = []
  distance = []
  vehicles = []
  supplierSelected: any = "";
  transactiondate: any = "";
  materialSelected : any = "";
  sitenameSelected : any = "";
  accountHolderSelected:any = "";
  itemOfWorkSelected :any = "";
  vehicleSelected :any = "";
  transporterSelected :any = "";
  unitSelected :any = "";
  distanceSelected: any ="";
  quantityGiven = '';
  vehicleNumSelected: any = "";
  NoofTripsEntered: any = "";
  NoofkmsEntered: any = "";
  TransCostEntered: any = "";
  invoiceNum: any = "";
  res :any = "";
  showMsg:any = false;
  remarks : any="";
  totaltransportcost : any;

  supplydataform = this.formBuilder.group({
    transactiondate: ['', Validators.required],
   
  });
  

  constructor(public dataService: DataService, private formBuilder: FormBuilder) { }

  ngOnInit() {
   // console.log(this.supplierSelected);
   // console.log(this.materialSelected);
    this.getSuppliersList();
    this.getSitenames();
    this.getItemOfWorkList();
    this.getAccountHolderList();
    this.getTransporterList();
    this.getMaterialList();
    this.getUnits();
    this.getDistance();
    this.getVehicleType();
    
  }
  
  
  getSuppliersList() {
    this.dataService.getSuppliersList().subscribe(res => {
      this.supplierList = res;
      //console.log("supplyform.ts : "+ JSON.stringify(this.supplierList));
    })
  }

  getTransporterList() {
    this.dataService.getTransporterList().subscribe(res => {
      this.transporterList = res;
     // console.log("supplyform.ts transporterList : "+ JSON.stringify(this.transporterList));
    })
  }

  getSitenames() {
    this.dataService.getSitenames().subscribe(res => {
      this.siteNames = res;
      //console.log("supplyform.ts siteNames : "+ JSON.stringify(this.siteNames));
    })
  }

  getItemOfWorkList() {
    this.dataService.getItemOfWorkList().subscribe(res => {
      this.itemOfWork = res;
      //console.log("supplyform.ts getItemOfWorkList : "+ JSON.stringify(this.itemOfWork));
    })
  }

  getAccountHolderList() {
    this.dataService.getAccountHolderList().subscribe(res => {
      this.accountHolders = res;
      //console.log("supplyform.ts getAccountHolderList : "+ JSON.stringify(this.accountHolders));
    })
  }

  getMaterialList() {
    this.dataService.getMaterialList().subscribe(res => {
      this.materialList = res;
      //console.log("supplyform.ts getMaterialList : "+ JSON.stringify(this.materialList));
    })
  }

  getMaterialCost(materialId,suppId,unitid) {
    this.dataService.getMaterialCost(materialId,suppId,unitid).subscribe(res => {
      this.materialCost1 = res;
      console.log("supplyform.ts materialCost : "+ JSON.stringify(this.materialCost1.rate));
    })
  }

  getUnits() {
    this.dataService.getUnits().subscribe(res => {
      this.units = res;
      //console.log("supplyform.ts units : "+ JSON.stringify(this.units));
    })
  }

  getDistance() {
    this.dataService.getDistance().subscribe(res => {
      this.distance = res;
      //console.log("supplyform.ts distance : "+ JSON.stringify(this.distance));
    })
  }

  getVehicleType() {
    this.dataService.getVehicleType().subscribe(res => {
      this.vehicles = res;
      //console.log("supplyform.ts vehicles : "+ JSON.stringify(this.vehicles));
    })
  }

  getMaterialRate(){
    console.log(this.supplierSelected);
    console.log(this.materialSelected);
  
    this.materialCost =  this.getMaterialCost(this.materialSelected,this.supplierSelected,this.unitSelected);
   
    console.log(this.materialCost.rate);
  }

  onSubmit(){
    console.log("Your Form Submitted *** ");
    var materialName ;
    
    for (let index = 0; index <= this.materialList.length; index++) {
      //console.log("---" + this.materialList[index]['id']);
      if(this.materialList[index]['id'] == this.materialSelected){
      materialName = this.materialList[index]['materialName'];
      
      break;
    }
  } 

  

    var flag = true;
    var object = {};
    if(this.transactiondate == ''){
      flag = false;
      alert("Please enter Transaction date");
      return false;
    }
    
    var GivenDate = this.transactiondate;
    var CurrentDate = new Date();
    GivenDate = new Date(GivenDate);

    if(GivenDate > CurrentDate){
      flag = false;
      alert('Transaction data cannot be greater than today date');
      return false;
  }

    if(this.sitenameSelected == ''){
      flag = false;
      alert("Please select the Site");
      return false;
    }

    if(this.itemOfWorkSelected == ''){
      flag = false;
      alert("Please select the Item of Work");
      return false;
    }
    if(this.accountHolderSelected == ''){
      flag = false;
      alert("Please select the AccountHolder");
      return false;
    }
    if(this.supplierSelected == ''){
      flag = false;
      alert("Please select the Supplier");
      return false;
    }

    if(this.materialSelected == ''){
      flag = false;
      alert("Please select a Material");
      return false;
    }
    

    if(this.unitSelected == ''){
      flag = false;
      alert("Please select a Unit");
      return false;
    }

     var isTransportReq = true;
    if(materialName == 'Asphalt'){
       isTransportReq = false;

    }else if (materialName == 'Emulsion'){
       isTransportReq = false;
    }else if (materialName == 'LDO'){
       isTransportReq = false;
    }
    console.log(isTransportReq)
    if(isTransportReq == true){

    if(this.transporterSelected == ''){
      flag = false;
      alert("Please select a Transporter");
      return false;
    }

    if(this.vehicleNumSelected == ''){
      flag = false;
      alert("Please provide Vehicle Number");
      return false;
    }

    if(this.vehicleSelected == ''){
      flag = false;
      alert("Please select type of Vehicle");
      return false;
    }

    if(this.NoofTripsEntered == '' && this.NoofkmsEntered == ''){
      flag = false;
      alert("Please enter either No Of trips or No of kms");
      return false;
    }

    if(this.distanceSelected == ''){
      flag = false;
      alert("Please select distance type");
      return false;
    }
    if(this.TransCostEntered == ''){
      flag = false;
      alert("Please enter the Transposrtation cost");
      return false;
    }
    if(this.vehicleSelected == ''){
      flag = false;
      alert("Please select type of Vehicle");
      return false;
    }
  }
    if(this.invoiceNum == ''){
      flag = false;
      alert("Please enter Invoice Number");
      return false;
    }
  

    object["trandate"] =  this.transactiondate;
    object["siteid"] = this.sitenameSelected;
    object["itemofworkid"]=this.itemOfWorkSelected;
    object["accholderid"]= this.accountHolderSelected;
    object["supplierid"]= this.supplierSelected;
    object["materialid"]= this.materialSelected;
    object["unitid"]= this.unitSelected;
    object["quantity"]= this.quantityGiven;
    if(!(materialName == '' || materialName == '' || materialName == '')){
    object["transporterid"]= this.transporterSelected;
    object["vehiclenumber"]= this.vehicleNumSelected;    
    object["vehicletypeid"]= this.vehicleSelected;
   // object["materialid"] = this.materialSelected;
    object["trips"] = this.NoofTripsEntered;
    object["kms"] = this.NoofkmsEntered;
    object["distanceid"] = this.distanceSelected;
    object["transportcost"] = this.TransCostEntered;
    object["flag"] = isTransportReq;
    }
    
    object["remarks"] = this.remarks;
    object["invoice"] = this.invoiceNum;
    var mit = this.materialCost1.id;
    object["materialcostid"] = mit;
    

    console.log(this.remarks,this.supplierSelected,this.sitenameSelected, 
    this.accountHolderSelected, this.itemOfWorkSelected,this.vehicleSelected,this.transporterSelected, this.unitSelected, this.distanceSelected);
if(flag == true){
    this.dataService.saveSuppyData(object).subscribe(res => {
      this.res = res;
      this.showMsg= true;
      console.log("supplyform.ts saveSuppyData : "+ this.showMsg);
    })
  }
  } 

  valuechange(newValue) {
    var trips = this.NoofTripsEntered;
    var kms = this.NoofkmsEntered;
    if(this.NoofTripsEntered == ''){
      trips =1;
    }
    if(this.NoofkmsEntered == ''){
      kms=1;
    }
    
    console.log(newValue)
    this.totaltransportcost = trips*kms* newValue;
  }

}