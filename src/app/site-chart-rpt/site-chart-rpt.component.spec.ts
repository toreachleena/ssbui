import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteChartRptComponent } from './site-chart-rpt.component';

describe('SiteChartRptComponent', () => {
  let component: SiteChartRptComponent;
  let fixture: ComponentFixture<SiteChartRptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteChartRptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteChartRptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
