import { Component, OnInit } from '@angular/core';

import { DataService } from '../data.service';

@Component({
  selector: 'app-site-chart-rpt',
  templateUrl: './site-chart-rpt.component.html',
  styleUrls: ['./site-chart-rpt.component.css']
})
export class SiteChartRptComponent implements OnInit {

  constructor(private dataService: DataService) { 

  }
  
  public pieChartLabels = ['Sales Q1', 'Sales Q2', 'Sales Q3', 'Sales Q4'];
  public pieChartData = [120, 150, 180, 90];
  public pieChartType = 'pie';

  ngOnInit() {
  }

}
