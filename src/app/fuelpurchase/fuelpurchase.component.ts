import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-fuelpurchase',
  templateUrl: './fuelpurchase.component.html',
  styleUrls: ['./fuelpurchase.component.css']
})
export class FuelpurchaseComponent implements OnInit {

  constructor(public dataService: DataService) { 
    
  }

  fuelList = [];
  fuelSelected : any = "";
  units = [];
  unitSelected :any = "";
  supplierList = []
  supplierSelected: any = "";
  accountHolders  = [];
  accountHolderSelected : any ;
  transactiondate : any ;
  quantityGiven = 0;
  rate = 0 ;
  invoiceNum : any ;
  amount = 0;
  res : any ;

  showMsg = false;

  ngOnInit() {
    
    this.getFuelTypes();
    this.getUnits();
    this.getSuppliersList();
    this.getAccountHolderList();
    this.showMsg = false;
  }


  getFuelTypes() {
    this.dataService.getFuelTypes().subscribe(res => {
      this.fuelList = res;
    })
  }

  getUnits() {
    this.dataService.getUnits().subscribe(res => {
      this.units = res;
      //console.log("supplyform.ts units : "+ JSON.stringify(this.units));
    })
  }

  getSuppliersList() {
    this.dataService.getDiesalSuppliers().subscribe(res => {
      this.supplierList = res;
      //console.log("supplyform.ts : "+ JSON.stringify(this.supplierList));
    })
  }

  getAccountHolderList() {
    this.dataService.getAccountHolderList().subscribe(res => {
      this.accountHolders = res;
      //console.log("supplyform.ts getAccountHolderList : "+ JSON.stringify(this.accountHolders));
    })
  }

  onSubmit(){
    console.log("Your Form Submitted FP *** " + this.transactiondate);

    var flag = true;
    var object = {};

    if(this.transactiondate == '' || this.transactiondate == undefined){
      flag = false;
      alert("Please enter Transaction date");
      return false;
    }
    
    var GivenDate = this.transactiondate;
    var CurrentDate = new Date();
    GivenDate = new Date(GivenDate);

    if(GivenDate > CurrentDate){
      flag = false;
      alert('Transaction data cannot be greater than today date');
      return false;
  }

  if(this.fuelSelected == ''){
    flag = false;
    alert("Please select a FuelType");
    return false;
  }

  if(this.unitSelected == ''){
    flag = false;
    alert("Please select a Unit");
    return false;
  }


  if(this.quantityGiven == 0){
    flag = false;
    alert("Please enter Quantity");
    return false;
  }

  if(this.accountHolderSelected == ''){
    flag = false;
    alert("Please select the AccountHolder");
    return false;
  }

  if(this.supplierSelected == ''){
    flag = false;
    alert("Please select the Supplier");
    return false;
  }

  if(this.rate == 0){
    flag = false;
    alert("Please enter Rate");
    return false;
  }

  if(this.invoiceNum == ''){
    flag = false;
    alert("Please enter InvoiceNumber");
    return false;
  }

  object["trandate"] =  this.transactiondate;
  object["fuelid"] = this.fuelSelected;
  object["rate"]=this.rate;
  object["accholderid"]= this.accountHolderSelected;
  object["supplierid"]= this.supplierSelected;
  object["invoice"]= this.invoiceNum;
  object["unitid"]= this.unitSelected;
  object["quantity"]= this.quantityGiven;

  console.log(object)

  this.dataService.saveFuelPurchased(object).subscribe(res => {
    this.res = res;
    this.showMsg= true;
    console.log("supplyform.ts saveSuppyData : "+ this.showMsg);
  })

  }

  valuechange(newValue) {
    this.rate = newValue;
    console.log(newValue)
    this.amount = this.quantityGiven * newValue;
  }

}
