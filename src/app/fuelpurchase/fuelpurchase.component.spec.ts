import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FuelpurchaseComponent } from './fuelpurchase.component';

describe('FuelpurchaseComponent', () => {
  let component: FuelpurchaseComponent;
  let fixture: ComponentFixture<FuelpurchaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FuelpurchaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FuelpurchaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
