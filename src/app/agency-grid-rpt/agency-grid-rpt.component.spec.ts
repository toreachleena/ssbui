import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgencyGridRptComponent } from './agency-grid-rpt.component';

describe('AgencyGridRptComponent', () => {
  let component: AgencyGridRptComponent;
  let fixture: ComponentFixture<AgencyGridRptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgencyGridRptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgencyGridRptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
