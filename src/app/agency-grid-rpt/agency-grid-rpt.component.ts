import { Component, OnInit } from '@angular/core';

import {  
  ColDef,  
  GridApi,  
  ColumnApi  
} from 'ag-grid-community'; 
import { DataService } from '../data.service';

@Component({
  selector: 'app-agency-grid-rpt',
  templateUrl: './agency-grid-rpt.component.html',
  styleUrls: ['./agency-grid-rpt.component.css']
})
export class AgencyGridRptComponent implements OnInit {

  public transportdata: [];
  public columnDefs: ColDef[];  
  // gridApi and columnApi  
  private api: GridApi;  
  private columnApi: ColumnApi;  

  onGridReady(params): void {  
    this.api = params.api;  
    this.columnApi = params.columnApi;  
    this.api.sizeColumnsToFit();  
  } 

  constructor(private dataService: DataService) {  
    this.columnDefs = this.createColumnDefs();  
} 


  title = 'Transport Data Report';

  // create column definitions  
 private createColumnDefs() {  
 return[
  {  
    headerName: 'Date',  
    field: 'transactiondate',  
    filter: true,  
    editable: true,  
    resizable:true,
    sortable: true  
  },
  {
    headerName: 'SiteName',  
    field: 'sitename',  
    filter: true,  
    editable: true,  
    resizable:true,
    sortable: true  
  },
  {  
    headerName: 'WorkName',  
    field: 'Abrevation',  
    filter: true,  
    sortable: true,  
    editable: true,  
    resizable:true,
    //cellRenderer: '<a href="edit-user">{{email}}</a>'  
  },
  {
    headerName: 'Transporter',  
    field: 'agencyname',  
    filter: true,  
    editable: true,  
    resizable:true,
    sortable: true  
  },
  {
    headerName: 'Vehicle',  
    field: 'vehicle_type',  
    filter: true,  
    editable: true,  
    resizable:true,
    sortable: true  
  },
  {
    headerName: 'VehicleNum',  
    field: 'vehiclenumber',  
    filter: true,  
    editable: true,  
    resizable:true,
    sortable: true  
  },
  {
    headerName: '#ofKms',  
    field: 'NoOfKms',  
    filter: true,  
    editable: true,  
    resizable:true,
    sortable: true  
  },
  {
    headerName: '#ofTrips',  
    field: 'NoOfTrips',  
    filter: true,  
    editable: true,  
    resizable:true,
    sortable: true  
  },
  {
    headerName: 'Distance',  
    field: 'distance_type',  
    filter: true,  
    editable: true,  
    resizable:true,
    sortable: true  
  },
  {
    headerName: 'TransportCost',  
    field: 'transportingcost',  
    filter: true,  
    editable: true,  
    resizable:true,
    sortable: true  
  },
  {
    headerName: 'Invoice',  
    field: 'invoicenumber',  
    filter: true,  
    editable: true,  
    resizable:true,
    sortable: true  
  }

 ]
 }



  ngOnInit() {

    this.dataService.getTransportData().subscribe(data => {  
      this.transportdata = data  ;
  })
  }

}
