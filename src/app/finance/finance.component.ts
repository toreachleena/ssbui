import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-finance',
  templateUrl: './finance.component.html',
  styleUrls: ['./finance.component.css']
})
export class FinanceComponent implements OnInit {
  
  res :any = "";
  
  agencynamelist:any = "";
  agencySelected : any = "";
  amountpaidto = []
  amountpaidSelected : any = "";
  modeofpaymentlist = []
  modeofpaymentlistSelected : any = "";

  transactiondate: any = "";
  bankName: any = "";
  chequenum: any = "";
  amount: any = "";
  gst: any = "";
  royalty: any = "";
  permit: any = "";
  showMsg = false;
  constructor(public dataService: DataService) { }

  ngOnInit() {
  this.getAgencyNameList();
  this.getAmountPaidToList();
  this.getModeOfPaymentList();

    
  }
  
  getAgencyNameList() {
    this.dataService.getAgencyNameList().subscribe(res => {
      this.agencynamelist = res;
      //console.log("financeform.ts : "+ JSON.stringify(this.agencynamelist));
    })
  }

    getAmountPaidToList() {
      this.dataService.getAgencyNameList().subscribe(res => {
        this.amountpaidto = res;
       // console.log("financeform.ts : "+ JSON.stringify(this.amountpaidto));
      })
  
  }

  getModeOfPaymentList() {
    this.dataService.getModeOfPaymentList().subscribe(res => {
      this.modeofpaymentlist = res;
     // console.log("financeform.ts : "+ JSON.stringify(this.modeofpaymentlist));
    })
  }

  onSubmit(){
  //  console.log("test submit");

    if(this.transactiondate == ''){
      alert("Please enter Transaction date");
      return false;
    }
    
    var GivenDate = this.transactiondate;
    var CurrentDate = new Date();
    GivenDate = new Date(GivenDate);

    if(GivenDate > CurrentDate){
      alert('Transaction data cannot be greater than today date');
      return false;
  }

  if(this.transactiondate == ''){
    alert("Please enter Transaction date");
    return false;
  }
  if(this.agencySelected == ''){
    alert("Please select an Agency");
    return false;
  }
  if(this.amountpaidSelected == ''){
    alert("Please selet Amount Paid to");
    return false;
  }
  if(this.modeofpaymentlistSelected == ''){
    alert("Please select Mode of Payment");
    return false;
  }

  if(this.modeofpaymentlistSelected != 'Cash'){
  if(this.bankName == ''){
    alert("Please enter BankName");
    return false;
  }
  if(this.chequenum == ''){
    alert("Please enter Cheque Number");
    return false;
  }
}
  if(this.amount == ''){
    alert("Please enter the Amount");
    return false;
  }
 /* if(this.gst == ''){
    alert("Please enter GST");
    return false;
  }
  if(this.royalty == ''){
    alert("Please enter royalty");
    return false;
  }

  if(this.permit == ''){
    alert("Please enter permit");
    return false;
  }*/


    var object = {};
    object["trandate"] = this.transactiondate;
    object["agencyname"] = this.agencySelected;
    object["amtpaidto"] = this.amountpaidSelected;
    object["mop"] = this.modeofpaymentlistSelected;
    object["bankname"] = this.bankName;
    object["chqrefno"] = this.chequenum;
    object["amt"] = this.amount;
    object["gst"] = this.gst;
    object["royalty"] = this.royalty;
    object["permit"] = this.permit;
  //  console.log(this.transactiondate +" -- " + this.agencySelected +" -- "+ this.amountpaidSelected + " -- "+this.modeofpaymentlistSelected + " --" + this.bankName + " -- " +this.chequenum+" -- " + this.amount + " -- " + this.gst + " -- " + this.permit +" -- " + this.royalty +" -- ");
    this.dataService.saveFinData(object).subscribe(res => {
      this.res = res;
      this.showMsg= true;
   //   console.log("supplyform.ts saveSuppyData : "+ res);
    })

  }
}