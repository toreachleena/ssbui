
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActionTabsComponent } from './action-tabs/action-tabs.component';
import { ContactCreateComponent } from './supplyform/supplyform.component';
import { FinanceComponent } from './finance/finance.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { ReportComponent } from './report/report.component';
import { MiscDataEntryComponent } from './misc-data-entry/misc-data-entry.component';
import { FuelpurchaseComponent } from './fuelpurchase/fuelpurchase.component';
import { FueldistComponent } from './fueldist/fueldist.component';
import { LoginComponent } from './login/login.component';
import { MachineryComponent } from './machinery/machinery.component';
import { FuelreportComponent } from './fuelreport/fuelreport.component';
import { MachineryreportComponent } from './machineryreport/machineryreport.component';
import { FinancereportComponent } from './financereport/financereport.component';

const routes: Routes = [
 //{path: "", pathMatch: "full" redirectTo: "/login"},
  {path: '', redirectTo: '/home', pathMatch: 'full'}, 
 //{ path: '**', component: PageNotFoundComponent },  // Wildcard route for a 404 page
   {path: "home", component: HomeComponent},
   {path: "header", component: HeaderComponent},
   {path: "supplyform", component: ContactCreateComponent},
   {path: "finance", component: FinanceComponent},
   {path: "action-tabs", component: ActionTabsComponent},
   {path: "report", component: ReportComponent},
   {path: "misc-data-entry", component: MiscDataEntryComponent},
   {path: "fuelpurchase", component: FuelpurchaseComponent}, 
   {path: "fueldist", component: FueldistComponent}, 
   {path: "login", component: LoginComponent},
   {path: "machinery", component: MachineryComponent},
   {path: "fuelreport", component: FuelreportComponent},
   {path: "machineryreport", component: MachineryreportComponent},
   {path: "financereport", component: FinancereportComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
