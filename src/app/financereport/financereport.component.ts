import { Component, OnInit } from '@angular/core';

import {  
  ColDef,  
  GridApi,  
  ColumnApi  
} from 'ag-grid-community'; 
import { DataService } from '../data.service';

@Component({
  selector: 'app-financereport',
  templateUrl: './financereport.component.html',
  styleUrls: ['./financereport.component.css']
})
export class FinancereportComponent implements OnInit {

  title = 'Finance Report';

  // row data and column definitions  
  public finacedata: [];  
  public columnDefs: ColDef[];  
  // gridApi and columnApi  
  private api: GridApi;  
  private columnApi: ColumnApi;  

  constructor(private dataService: DataService) { 
 
    this.columnDefs = this.createColumnDefs();  
  }

  ngOnInit() {
    this.dataService.getFinanceReport().subscribe(data => {  
      this.finacedata = data  ;
  })
  }

  onGridReady(params): void {  
    this.api = params.api;  
    this.columnApi = params.columnApi;  
    this.api.sizeColumnsToFit();  
  } 

  /*
  "chq_refNo": "CHQ11022",
        "amount": 35000,
        "transactiondate": "2021-07-12",
        "gst": 0,
        "royalty": 0,
        "modeofpayment": 2,
        "actualamountpaid_id": 58,
        "total": 35000,
        "amountpaidto": "Lingamurty",
        "permit": 0,
        "agencyname_id": 48,
        "id": 5,
        "bankname": "HDFC Bank",
        "agencyname": "Dheeraj"
        p.payment_type
        */
  private createColumnDefs() {  
    return [ 
      {  
        headerName: 'Date',  
        field: 'transactiondate',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'Agency',  
        field: 'agencyname',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'AmountPaidTo',  
        field: 'amountpaidto',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'PaymentMode',  
        field: 'payment_type',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'Bank',  
        field: 'bankname',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'Cheque',  
        field: 'chq_refNo',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'Amount',  
        field: 'amount',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'GST',  
        field: 'gst',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'Royalty',  
        field: 'royalty',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'Permit',  
        field: 'permit',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'Total',  
        field: 'total',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      }

    ]
  }

}
