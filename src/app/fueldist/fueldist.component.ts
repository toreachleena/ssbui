import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-fueldist',
  templateUrl: './fueldist.component.html',
  styleUrls: ['./fueldist.component.css']
})
export class FueldistComponent implements OnInit {

  fuelList = [];
  fuelSelected : any = "";
  vehicles = [] ;
  vehicleSelected :any = "";
  accountHolders = [];
  accountHolderSelected : any ="";
  agencyList = [];
  agencySelected : any = "";
  transactiondate : any ;
  units = [];
  unitSelected :any = "";
  quantityGiven = 0 ;
  vehicleNumSelected : any ="";
  showMsg = false;
  res = [];
  remainingqty = 0.0;


  constructor(public dataService: DataService) { 

  }

  ngOnInit() {
    this.getFuelTypes();
    this.getVehicleType();
    this.getAccountHolderList();
    this.getTransporterList();
    this.getUnits();
  }

  getFuelTypes() {
    this.dataService.getFuelTypes().subscribe(res => {
      this.fuelList = res;
    })
  }

  getUnits() {
    this.dataService.getUnits().subscribe(res => {
      this.units = res;
      //console.log("supplyform.ts units : "+ JSON.stringify(this.units));
    })
  }

  getVehicleType() {
    this.dataService.getVehicleType().subscribe(res => {
      this.vehicles = res;
      //console.log("supplyform.ts vehicles : "+ JSON.stringify(this.vehicles));
    })
  }

  getAvailableFuelQty(){

    this.dataService.getAvailableFuelQty(this.fuelSelected).subscribe(res => {
       this.remainingqty = res;
      console.log("this.remainingqty "+ this.remainingqty);
    })

    console.log(this.fuelSelected);
  }

  onSubmit(){
    console.log("Your Form Submitted FD *** " + this.transactiondate);

    var flag = true;
    var object = {};

    if(this.transactiondate == '' || this.transactiondate == undefined){
      flag = false;
      alert("Please enter Transaction date");
      return false;
    }
    
    var GivenDate = this.transactiondate;
    var CurrentDate = new Date();
    GivenDate = new Date(GivenDate);

    if(GivenDate > CurrentDate){
      flag = false;
      alert('Transaction data cannot be greater than today date');
      return false;
  }

  if(this.fuelSelected == ''){
    flag = false;
    alert("Please select a FuelType");
    return false;
  }

  if(this.vehicleNumSelected == ''){
    flag = false;
    alert("Please provide Vehicle Number");
    return false;
  }
  
  if(this.vehicleSelected == ''){
    flag = false;
    alert("Please select type of Vehicle");
    return false;
  }

  if(this.unitSelected == ''){
    flag = false;
    alert("Please select a Unit");
    return false;
  }


  if(this.quantityGiven == 0){
    flag = false;
    alert("Please enter Quantity");
    return false;
  }

  if(this.agencySelected == ''){
    flag = false;
    alert("Please select the Transporter");
    return false;
  }

  if(this.accountHolderSelected == ''){
    flag = false;
    alert("Please select the AccountHolder");
    return false;
  }

  var rem = this.remainingqty;

  if(this.quantityGiven > rem){
    alert("Quantity to be used must be less than remaining quantity");
    return false;
  }
  object["useddate"] =  this.transactiondate;
  object["fuelid"] = this.fuelSelected;
  object["vehiclenumber"]=this.vehicleNumSelected;
  object["accholderid"]= this.accountHolderSelected;
  object["vehicleSelected"]= this.vehicleSelected;
  object["quantity"]= this.quantityGiven;
  object["unitid"]= this.unitSelected;
  object["transporter"]= this.agencySelected;

  console.log(object)
  
  this.dataService.saveFuelDistribution(object).subscribe(res => {
    this.res = res;
    this.showMsg= true;
    console.log("saveFuelDistribution: "+ this.showMsg);
  })

}

  getAccountHolderList() {
    this.dataService.getAccountHolderList().subscribe(res => {
      this.accountHolders = res;
      //console.log("supplyform.ts getAccountHolderList : "+ JSON.stringify(this.accountHolders));
    })
  }
  getTransporterList() {
    this.dataService.getTransporterList().subscribe(res => {
      this.agencyList = res;
     // console.log("supplyform.ts transporterList : "+ JSON.stringify(this.transporterList));
    })
  }

}
