import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FueldistComponent } from './fueldist.component';

describe('FueldistComponent', () => {
  let component: FueldistComponent;
  let fixture: ComponentFixture<FueldistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FueldistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FueldistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
