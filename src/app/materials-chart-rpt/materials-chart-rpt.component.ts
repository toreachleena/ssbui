import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-materials-chart-rpt',
  templateUrl: './materials-chart-rpt.component.html',
  styleUrls: ['./materials-chart-rpt.component.css']
})
export class MaterialsChartRptComponent implements OnInit {

  constructor() { }

  public doughnutChartLabels = ['Sales Q1', 'Sales Q2', 'Sales Q3', 'Sales Q4'];
  public doughnutChartData = [120, 150, 180, 90];
  public doughnutChartType = 'doughnut';

  ngOnInit() {
  }

}
