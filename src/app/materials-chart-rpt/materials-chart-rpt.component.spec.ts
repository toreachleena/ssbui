import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialsChartRptComponent } from './materials-chart-rpt.component';

describe('MaterialsChartRptComponent', () => {
  let component: MaterialsChartRptComponent;
  let fixture: ComponentFixture<MaterialsChartRptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialsChartRptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialsChartRptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
