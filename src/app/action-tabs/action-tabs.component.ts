import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';


@Component({
  selector: 'app-action-tabs',
  templateUrl: './action-tabs.component.html',
  styleUrls: ['./action-tabs.component.css']
})
export class ActionTabsComponent implements OnInit {

  res : any = "";

  agencyList = [];
  activeTab  = "tab1";

  agency : any = ""
  agency1: any = ""
  agencyMSelected : any = "";
  supplierList = [];
  //agencyNames = [];
  agencySelected : any = "";
  supplierSelected : any = "";
  typeofworklist = []
  typeofworkSelected : any = "";
  materialSelected : any = "";
  materialList = []
  supplierListM = []
  supplierMSelected : any = "";
  sitenamelist = []
  sitenameSelected : any = "";
  materialCost: any = "";
  sitenameSelected1 : any = "";


  site:any ="";
  site1:any ="";
  typeofworkMSelected : any = "";
  sitename: any = "";
  details: any = "";
  startdate: any ="";
  estcost: any ="";
  estcostnogst: any ="";
  materialname:any ="";

  issupplier: any = "";
  istransporter: any = "";
  isdieselsupplier : any ="";
  isaccholder: any = "";
  newagency: any = "";
  email: any = "";
  telphone: any = "";
  address: any = "";
  units= [];
  unitSelected: any = "";
  materialCost1:any = "";
  newrate:any = "";
  trandate :any ="";
  vehicle : any="";

  flag1:any = "";
  flag2:any = "";
  flag3:any = "";
  flagA:any = "";
  flagB:any = "";
  flagS:any ="";
  showMsg= false;
  showMsg1= false;
  showMsg2= false;
  showMsg4= false;
  showMsg5= false;
  showMsg6 = false;
  showMsg7 = false;

  flag1M:any = "";
  flag2M:any = "";
  flag3M:any = "";
  flag4M:any = "";

  contacts ;
  selectedContact;
  constructor(public dataService: DataService) { }

  ngOnInit() {
     
     
  }

  getAgencyList() {
    this.dataService.getAgencies().subscribe(res => {
      this.agencyList = res;
     // console.log("supplyform.ts : "+ JSON.stringify(this.agencyList));
    })
  }

  getMaterialRate(){
   // console.log(this.materialSelected);
   // console.log(this.supplierMSelected);
    this.materialCost =  this.getMaterialCost(this.materialSelected,this.supplierMSelected,this.unitSelected);
  }

  getSuppliersList() {
    this.dataService.getSuppliersList().subscribe(res => {
      this.supplierList = res;
      console.log("supplyform.ts : "+ JSON.stringify(this.supplierList));
    })
  }

  
  getSiteNameList() {
    this.dataService.getSiteNameList().subscribe(res => {
      this.sitenamelist = res;
    //  console.log("supplyform.ts : "+ JSON.stringify(this.sitenamelist));
    })
  }
  getTypeOfWorkList() {
    this.dataService.getTypeOfWorkList().subscribe(res => {
      this.typeofworklist = res;
      console.log("supplyform.ts : "+ JSON.stringify(this.typeofworklist));
    })
  }

  getMaterialList() {
    this.dataService.getMaterialList().subscribe(res => {
      this.materialList = res;
     //console.log("supplyform.ts getMaterialList : "+ JSON.stringify(this.materialList));
    })
  }

  getUnits() {
    this.dataService.getUnits().subscribe(res => {
      this.units = res;
      //console.log("supplyform.ts units : "+ JSON.stringify(this.units));
    })
  }

  getSuppliersListM() {
    this.dataService.getSuppliersListM().subscribe(res => {
      this.supplierListM = res;
 //     console.log("supplyform.ts : "+ JSON.stringify(this.supplierListM));
    })
  }
  public selectContact(contact){
    this.selectedContact = contact;
  }

  addAgency(){ 
    //just added console.log which will display the event details in browser on click of the button.
    //console.log("clickEvent");
   // console.log(this.issupplier);
   // console.log(this.isaccholder);
   // console.log(this.istransporter);
   // console.log(this.newagency);
   // console.log(this.email);
   // console.log(this.telphone);
    //console.log(this.address);

    if(this.issupplier == ''  && this.isaccholder == '' && this.istransporter== '' && this.isdieselsupplier ==''){
      alert("Please select atleast one checkbox for  Supplier /Transporter / AccountHolder / Diesel aupplier");
      return false;
    }
    if(this.newagency == ''){
      alert("Please enter AgencyName");
      return false;
    }
   
    if(this.telphone == ''){
      alert("Please enter Ph Number");
      return false;
    }
    if(this.address == ''){
      alert("Please enter addressr");
      return false;
    }
   

    var object = {};
    if(this.issupplier == true)
    object["supplierflag"] =  this.issupplier;
    if(this.isaccholder == true)
    object["accholderflag"] =  this.isaccholder;
    if(this.istransporter == true)
    object["transporterflag"] =  this.istransporter;
    if(this.isdieselsupplier == true){
    object["dieselsupplier"] =  this.isdieselsupplier;
    object["accholderflag"] =  true;
    }
    object["email"] = this.email;
    object["phnumber"] = this.telphone;
    object["address"] = this.address;
    object["agencyname"] = this.newagency;
    
    this.dataService.saveAgencyData(object).subscribe(res => {
      this.res = res;
      this.showMsg= true;
   //   console.log(" action  ts saveAgencyData : "+ res);
    })

 }

 getMaterialCost(materialId,suppId,unitid) {
  this.dataService.getMaterialCost(materialId,suppId,unitid).subscribe(res => {
    this.materialCost1 = res;
   // console.log("supplyform.ts materialCost : "+ JSON.stringify(this.materialCost1.rate));
  })
}

 updateAgency(){ 
  /*ust added console.log which will display the event details in browser on click of the button.
  console.log("updateAgency");
  console.log(this.issupplier);
  console.log(this.isaccholder);
  console.log(this.istransporter);
  console.log(this.email);
  console.log(this.telphone);
  console.log(this.address);*/

  if(this.flag1M == ''  && this.flag2M == '' && this.flag3M == ''){
    alert("Please select atleast one checkbox for  Supplier /Transporter / AccountHolder");
    return false;
  }
  if(this.agencyMSelected == ''){
    alert("Please select AgencyName");
    return false;
  }
  
  if(this.agency1.phoneNumber == ''){
    alert("Please enter Ph Number");
    return false;
  }
  if(this.agency1.address == ''){
    alert("Please enter addressr");
    return false;
  }

  var object = {};
  if(this.flag1M == true)
  object["supplierflag"] =  this.flag1M;
  if(this.flag2M == true)
  object["accholderflag"] =  this.flag2M;
  if(this.flag3M == true)
  object["transporterflag"] =  this.flag3M;
  if(this.flag4M == true)
  object["dieselsupplier"] =  this.flag4M;
  
  object["email"] = this.agency1.email;
  object["phnumber"] = this.agency1.phoneNumber;
  object["address"] = this.agency1.address;
  object["id"] = this.agencyMSelected;
  
  this.dataService.saveAgencyData(object).subscribe(res => {
    this.res = res;
    this.showMsg1 = true;
    
   // console.log(" action  ts saveAgencyData : "+ res);
  })

}

 getAgencyDetails(){
 // console.log("*** " + this.agencySelected);
  this.dataService.getAgencyDetails(this.agencyMSelected).subscribe(res =>{
    this.agency1 = res;
    if(this.agency1.supplierFlag == 'true'){
        this.flag1M = 'checked'
    }else {
      this.flag1M = ''
    }
    if(this.agency1.accountHolderFlag == 'true'){
      this.flag2M = 'checked'
    }else {
      this.flag2M = ''
    }
    if(this.agency1.transposterFlag == 'true'){
      this.flag3M = 'checked'
    }else {
      this.flag3M = ''
    }
    if(this.agency1.dieselSupplier == 'true'){
      this.flag4M = 'checked'
    }else {
      this.flag4M = ''
    }
  })
 }

 loadme(){
   //alert("LOADDDDDDDD");
 }

 addSiteDetails(){
  
  

  if(this.sitename == ''){
    alert("Please enter Site Name");
    return false;
  }
  if(this.details == ''){
    alert("Please enter Site Details");
    return false;
  }
  if(this.estcost == '' && this.estcostnogst== ''){
    alert("Please enter one of the AOC");
    return false;
  }

  

  if(this.typeofworkSelected == ''){
    alert("Please select Work Category");
    return false;
  }

  if(this.startdate == ''){
    alert("Please select STARTDATE");
    return false;
  }
  var object = {};
  object["sitename"] =  this.sitename;
  object["workname"] =  this.typeofworkSelected;
  object["details"] =  this.details;
  object["startdate"] =  this.startdate;

  //var cost = this.estcost*100000;
  if(this.estcost == 'null' || this.estcost == '' ){
    object["estcost"] =  0.0;
  }
  else{
    object["estcost"] =  this.estcost;
  }

  if(this.estcostnogst == 'null' || this.estcostnogst == '' ){
    object["estcostnogst"] =  0.0;
  }else{
    object["estcostnogst"] =  this.estcostnogst;
  }
  
  this.dataService.saveNewSite(object).subscribe(res => {
    this.res = res;
    this.showMsg= true;
  //  console.log(" action  ts saveNewSite : "+ res);
  })

 }

 getSiteDetails(){
  //console.log("*** " + this.sitenameSelected1);
  this.dataService.getSiteDetails(this.sitenameSelected1).subscribe(res =>{
    this.site1 = res;
    this.site1.estcost = this.site1.estcost / 100000;
    this.typeofworkMSelected = this.site1.workname;
    if(this.site1.completed == true){
      this.flagS="checked";
    }
 })
}

updateSiteDetails(){

  if(this.site1.details == ''){
    alert("Please enter Site Details");
    return false;
  }
  if(this.site1.estcost == '' && this.site1.estcostnogst == ''){
    alert("Please enter AOC in lakhs");
    return false;
  }

  if(this.typeofworkMSelected == ''){
    alert("Please select Work Category");
    return false;
  }

  if(this.site1.startdat == ''){
    alert("Please select STARTDATE");
    return false;
  }

  var object = {};
  if(this.flagS == true){
    object["completed"] =  this.flagS;
  }else{
    object["completed"] =  false;
  }
  object["id"] =  this.sitenameSelected1;
  object["workname"] =  this.typeofworkMSelected;
  object["details"] =  this.site1.details;
  object["startdate"] =  this.site1.startdate;

  //var cost1 = this.site1.estcost*100000;
  object["estcostnogst"] =  this.site1.estcostnogst;
  object["estcost"] =  this.site1.estcost;

  this.dataService.updateSite(object).subscribe(res => {
    this.res = res;
    this.showMsg4= true;
 //   console.log(" action  ts updateNSite : "+ res);
  })
 
}

addMaterial(){

  var object = {};
  object["materialname"] =  this.materialname;

  if(this.materialname == ''){
    alert("Please enter MaterialName");
    return false;
  }

  this.dataService.addMaterial(object).subscribe(res => {
    this.res = res;
    this.showMsg5= true;
 //   console.log(" action  ts addMaterial : "+ res);
  })

}

search(activeTab){

  this.activeTab = activeTab;
 // console.log("In search " + activeTab);
  if(activeTab == 'tab1'){

  }else if (activeTab == 'tab2'){
    
     this.getAgencyList();
    

  }else if (activeTab == 'tab3'){

    this.getTypeOfWorkList();
     
    
  }else if (activeTab == 'tab4'){

    this.getTypeOfWorkList();
    
     this.getSiteNameList();
     
    
  }else if (activeTab == 'tab5'){

    
  }else if (activeTab == 'tab6'){

    
     this.getMaterialList();
     this.getSuppliersListM();
     
     this.getUnits();

  this.showMsg6 = false;
 
    
  }else if (activeTab == 'tab7'){
   
    this.vehicle='';
  this.showMsg7 = false;
    
  }else if (activeTab == 'tab8'){
    
  }
}

updateMaterialRate(){

  //console.log("updateMaterialRate");

  var object = {};
  object["mid"] =  this.materialSelected;
  object["supplier"] =  this.supplierMSelected;
  object["unit"] =  this.unitSelected;
  object["rate"] =  this.newrate;
  object["trandate"] =  this.trandate;

  if(this.materialSelected == ''){
    alert("Please select a Material");
    return false;
  }

  if(this.supplierMSelected == ''){
    alert("Please select a Supplier");
    return false;
  }

  if(this.unitSelected == ''){
    alert("Please select a Unit");
    return false;
  }

  if(this.newrate == ''){
    alert("Please provide a rate");
    return false;
  }

  if(this.trandate == ''){
    alert("Please select Date");
    return false;
  }

  this.dataService.updateMaterialRate(object).subscribe(res => {
    this.res = res;
    this.showMsg6= true;
   // console.log(" action  ts addMaterial : "+ res);
  })


}

addVehicle(){

 // console.log("updateMaterialRate");
  
  var object = {};
  object["vehicle"] =  this.vehicle;
  
  if(this.vehicle == ''){
    alert("Please Enter Vehicle Name");
    return false;
  }
  this.dataService.addVehicle(object).subscribe(res => {
    this.res = res;
    this.showMsg7= true;
  //  console.log(" action  ts addVehicle : "+ res);
  })

}


}