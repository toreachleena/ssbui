import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionTabsComponent } from './action-tabs.component';

describe('ActionTabsComponent', () => {
  let component: ActionTabsComponent;
  let fixture: ComponentFixture<ActionTabsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionTabsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
