import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteGridRptComponent } from './site-grid-rpt.component';

describe('SiteGridRptComponent', () => {
  let component: SiteGridRptComponent;
  let fixture: ComponentFixture<SiteGridRptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteGridRptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteGridRptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
