import { Component, OnInit } from '@angular/core';

import { DataService } from '../data.service';
import { DelButtonRendererComponent } from '../renderer/del-button-renderer-component';
import { NumericEditorComponent } from '../renderer/numeric-editor.component';
import { NumberFormatterComponent } from '../renderer/NumberFormatterComponent';
import { CheckboxRenderer } from '../renderer/checkbox-renderer.component';





import {  
  ColDef,  
  GridApi,  
  ColumnApi  
} from 'ag-grid-community'; 

@Component({
  selector: 'app-site-grid-rpt',
  templateUrl: './site-grid-rpt.component.html',
  styleUrls: ['./site-grid-rpt.component.css']
})
export class SiteGridRptComponent implements OnInit {

  frameworkComponents;

  constructor(private dataService: DataService) {

    this.frameworkComponents = {
      buttonRenderer: DelButtonRendererComponent ,
      numericEditorComponent : NumericEditorComponent,
      numberFormatterComponent :NumberFormatterComponent,
      checkboxRenderer : CheckboxRenderer
    }

    this.columnDefs = this.createColumnDefs();  
    this.columnDefsS = this.createColumnDefsS();
    this.columnDefsT = this.createColumnDefsT();
    this.columnDefsM = this.createColumnDefsM();
    this.columnDefsMc = this.createColumnDefsMc();
   }

  del1=false;
  success1;
  title = 'Site Report';
  public siteData: [];
  public supplyData: [];
  public transportData: [];
  public miscData: [];
  public machineData: [];
  public machinaryData: [];
  public heading:any;
  public showsupply= false;
  public showtransport= false;
  public showmisc= false;
  public showmachine=false;
  rowDataClicked1 ={};

   // gridApi and columnApi  
   private api: GridApi;  
   private columnApi: ColumnApi;  
   public columnDefs: ColDef[];

   // gridApi and columnApi  
  private apiS: GridApi;  
  private columnApiS: ColumnApi;  
  public columnDefsS: ColDef[];

  // gridApi and columnApi  
  private apiT: GridApi;  
  private columnApiT: ColumnApi;  
  public columnDefsT: ColDef[];

  // gridApi and columnApi  
  private apiM: GridApi;  
  private columnApiM: ColumnApi;  
  public columnDefsM: ColDef[];

  // gridApi and columnApi  
  private apiMc: GridApi;  
  private columnApiMc: ColumnApi;  
  public columnDefsMc: ColDef[];
  
  ngOnInit() {
      this.dataService.getSiteSummary().subscribe(data => {  
      this.siteData = data  ;
    })

  }

  onGridReady(params): void {  
    this.api = params.api;  
    this.columnApi = params.columnApi;  
    this.api.sizeColumnsToFit();  
  } 

  onGridReady1(params) {
    this.apiS = params.api;  
    this.columnApiS = params.columnApi;  
    this.apiS.sizeColumnsToFit(); 
  }

  onGridReady2(params) {
    this.apiT = params.api;  
    this.columnApiT = params.columnApi;  
    this.apiT.sizeColumnsToFit(); 
  }

  onGridReady3(params) {
    this.apiM = params.api;  
    this.columnApiM = params.columnApi;  
    this.apiM.sizeColumnsToFit(); 
  }

  onGridReady4(params) {
    this.apiMc = params.api;  
    this.columnApiMc = params.columnApi;  
    this.apiMc.sizeColumnsToFit(); 
  }

  /*
         "siteId": 16,
        "siteName": "Office Site",
        "totalSupplyCost": 215000,
        "totalTransportCost": 144857,
        "totalMiscExpense": null,
        "grandTotal": null,
        "estimatedCost": 24000000
*/

  private createColumnDefs() {  
    return [ 
      {  
        headerName: 'Site',  
        field: 'siteName',  
        filter: true,  
       
        resizable:true,
        sortable: true  
    },
      {  
        headerName: 'SupplyCost',  
        field: 'totalSupplyCost',  
        filter: true,  
        cellRenderer: this.createHyperLink.bind(this),
        resizable:true,
        sortable: true  
    },
    {  
      headerName: 'Transport Cost',  
      field: 'totalTransportCost',  
      filter: true,  
      cellRenderer: this.createHyperLink1.bind(this),
      resizable:true,
      sortable: true  
  },
  {  
    headerName: 'Misc Amount',  
    field: 'totalMiscExpense',  
    filter: true,  
    cellRenderer: this.createHyperLink2.bind(this), 
    resizable:true,
    sortable: true  
},
{  
  headerName: 'Machinery Cost',  
  field: 'totalMachinerycExpense',  
  filter: true,  
  cellRenderer: this.createHyperLink3.bind(this), 
  resizable:true,
  sortable: true  
},
{  
  headerName: 'Total Expenditure',  
  field: 'grandTotal',  
  filter: true,  
  editable: false,  
  resizable:true,
  sortable: true  
}
,
{  
  headerName: 'Estimated Cost',  
  field: 'estimatedCost',  
  filter: true,  
  editable: false,  
  resizable:true,
  sortable: true  
}
    ]
  }

  /*
TotalCost: 15875
accountholder: "Model Stone Crusher Antapur"
accountholderid: 59
materialcostid: 1951
materialid: 3
materialname: "20 MM"
quantity: 25
rate: 635
siteid: 16
supplier: "Model Stone Crusher Antapur"
supplierid: 59
transactiondate: "2020-10-19"
unitid: 2
unitname: "Ton"
  */
  private createColumnDefsS() {  
    return [
      {  
        headerName: 'Date',  
        field: 'transactiondate',  
        filter: true,  
        resizable:true,
        sortable: true  
    },
    {  
      headerName: 'AccountHolder',  
      field: 'accountholder',  
      filter: true,  
      resizable:true,
      sortable: true  
  },
  {  
    headerName: 'Supplier',  
    field: 'supplier',  
    filter: true,  
    resizable:true,
    sortable: true  
},
{  
  headerName: 'Material',  
  field: 'materialname',  
  filter: true,  
  resizable:true,
  sortable: true  
},
{  
  headerName: 'Unit',  
  field: 'unitname',  
  filter: true,  
  resizable:true,
  sortable: true  
},
{  
  headerName: 'Rate',  
  field: 'rate',  
  filter: true,  
  resizable:true,
  sortable: true  
},
{  
  headerName: 'Qty',  
  field: 'quantity',  
  filter: true,  
  resizable:true,
  sortable: true ,
  editable: true,
  cellEditor: 'numericEditorComponent',
},{  
  headerName: 'TotalCost',  
  field: 'TotalCost',  
  filter: true,  
  resizable:true,
  sortable: true  
},
{  
  headerName: 'Remarks',  
  field: 'remarks',  
  filter: true,  
  resizable:true,
  sortable: true  
},
{  
    headerName: 'Action',
    resizable:true,
         cellRenderer: 'buttonRenderer',
         cellRendererParams: {
           onClick: this.onBtnClick2.bind(this),
           label: 'Delete'
         }
}
    ]
  }

  /*
   Abrevation: "Subgrade"
Invoicenumber: "59"
ItemOfWorkID: 6
NoOfkms: 0
NoOftrips: 1
agencyname: "Satyappa"
distance_type: "Trip"
siteid: 17
transactiondate: "2020-11-18"
transporterid: 70
transportingcost: 0
vehicle_type: "Tipper 10 Wheeler"
vehiclenumber: "KA 34 A 9191"
  */
  private createColumnDefsT() {  
    return [
      {  
        headerName: 'Date',  
        field: 'transactiondate',  
        filter: true,  
        resizable:true,
        sortable: true  
    },
    {  
      headerName: 'Transporter',  
      field: 'agencyname',  
      filter: true,  
      resizable:true,
      sortable: true  
  },
  {  
    headerName: 'Work',  
    field: 'Abrevation',  
    filter: true,  
    resizable:true,
    sortable: true  
},
{  
  headerName: 'Vehicle',  
  field: 'vehicle_type',  
  filter: true,  
  resizable:true,
  sortable: true  
},
{  
  headerName: 'Vehicle#',  
  field: 'vehiclenumber',  
  filter: true,  
  resizable:true,
  sortable: true  
},
{  
  headerName: '#Trips',  
  field: 'NoOftrips',  
  filter: true,  
  resizable:true,
  sortable: true  
},{  
  headerName: 'Distance',  
  field: 'distance_type',  
  filter: true,  
  resizable:true,
  sortable: true  
},{  
  headerName: '#Kms',  
  field: 'NoOfkms',  
  filter: true,  
  resizable:true,
  sortable: true  
},
{  
  headerName: 'Remarks',  
  field: 'remarks',  
  filter: true,  
  resizable:true,
  sortable: true  
},
{  
    headerName: 'Action',
    resizable:true,
         cellRenderer: 'buttonRenderer',
         cellRendererParams: {
           onClick: this.onBtnClick1.bind(this),
           label: 'Delete'
         }
}
  ]
}

/*{
  "trandate": 1622745000000,
  "siteid": 10,
  "amount": 1200,
  "expensefor": "Test1"
}*/

private createColumnDefsM() {  
  return [
    {
      headerName: "Edit",
      field: "registered",
      cellRenderer: "checkboxRenderer"
    },
    {  
      headerName: 'Date',  
      field: 'trandate',  
      filter: true,  
      resizable:true,
      sortable: true  
  },
  {  
    headerName: 'ExpenseFor',  
    field: 'expensefor',  
    editable : true,
    filter: true,  
    resizable:true,
    sortable: true  
},
{  
  headerName: 'Amount',  
  field: 'amount',  
  editable : true,
  filter: true,  
  resizable:true,
  sortable: true  
},{  
  headerName: 'Delete',
     resizable:true,
       cellRenderer: 'buttonRenderer',
       cellRendererParams: {
        onClick: this.onDeleteButtonClick1.bind(this),
        label: 'Delete'
        },
    },
    {  
      headerName: 'Save',
         resizable:true,
           cellRenderer: 'buttonRenderer',
           cellRendererParams: {
            onClick: this.onDeleteButtonClicksave.bind(this),
            label: 'Save'
            },
        },
]}

/*
"vehicletype": 3,
        "accountholdername": "Amma Iron And Steel",
        "miscamount": 100,
        "trandate": "2021-07-09",
        "vehicle_type": "Tipper 12 Wheeler",
        "vehiclenum": "KA340988",
        "agencyid": 43,
        "duration": "days",
        "qty": 5,
        "Total": 7850,
        "sitename": "Auto Nagar Site 1",
        "siteid": 10,
        "accountholder": 44,
        "id": 3,
        "operatorbata": 250,
        "agencyname": "Aishwarya Stone Crusher",
        "unitrate": 1500
*/

private createColumnDefsMc() {  
  return [
    {  
      headerName: 'Date',  
      field: 'trandate',  
      filter: true,  
      resizable:true,
      sortable: true  
  },
  {  
    headerName: 'AccountHolder',  
    field: 'accountholdername',  
    filter: true,  
    resizable:true,
    sortable: true  
},
{  
  headerName: 'Agency',  
  field: 'agencyname',  
  filter: true,  
  resizable:true,
  sortable: true  
},
{  
  headerName: 'Vehicle#',  
  field: 'vehiclenum',  
  filter: true,  
  resizable:true,
  sortable: true  
},
{  
  headerName: 'Vehicle',  
  field: 'vehicle_type',  
  filter: true,  
  resizable:true,
  sortable: true  
},
{  
  headerName: 'Qty',  
  field: 'qty',  
  filter: true,  
  resizable:true,
  sortable: true  
},
{  
  headerName: 'Duration',  
  field: 'duration',  
  filter: true,  
  resizable:true,
  sortable: true  
},
{  
  headerName: 'Rate',  
  field: 'unitrate',  
  filter: true,  
  resizable:true,
  sortable: true  
},
{  
  headerName: 'OperatorBata',  
  field: 'operatorbata',  
  filter: true,  
  resizable:true,
  sortable: true  
},
{  
  headerName: 'ExpenseFor',  
  field: 'miscdetails',  
  filter: true,  
  resizable:true,
  sortable: true  
},
{  
  headerName: 'Misc',  
  field: 'miscamount',  
  filter: true,  
  resizable:true,
  sortable: true  
},
{  
  headerName: 'Total',  
  field: 'Total',  
  filter: true,  
  resizable:true,
  sortable: true  
},{  
  headerName: 'Delete',
     resizable:true,
       cellRenderer: 'buttonRenderer',
       cellRendererParams: {
        onClick: this.onDeleteButtonClick2.bind(this),
        label: 'Delete'
        },
    },
  ]
}

  createHyperLink(params): any {
    
    if (!params.data) { return; }
    const spanElement = document.createElement('span');
    spanElement.innerHTML = `<a href="#siteModal" data-toggle="modal" data-target="#siteModal" > ${params.value} </a> `;
    spanElement.addEventListener('click', ($event) => {
      $event.preventDefault();
      this.supplydetails(params.data);
    });
    return spanElement;
  }

  supplydetails(rowdata){
    
    console.log(rowdata['siteId']);
    console.log(rowdata['siteName']);

    this.heading = "Supplier transactions Details for Site : "+rowdata['siteName'];
     this.showsupply = true;
     this.showtransport= false;
     this.showmisc =false;
     this.showmachine =false;

    this.dataService.getSiteSupplyDetails(rowdata['siteId']).subscribe(data => {  
      this.supplyData = data  ;

       // console.log(this.supplyData);
  })  
 

  }

  createHyperLink1(params): any {
    
    if (!params.data) { return; }
    const spanElement = document.createElement('span');
    spanElement.innerHTML = `<a href="#siteModal" data-toggle="modal" data-target="#siteModal"  >  ${params.value} </a> `;
    spanElement.addEventListener('click', ($event) => {
      $event.preventDefault();
      this.transportdetails(params.data);
    });
    return spanElement;
  }

  transportdetails(rowdata){
    console.log(rowdata);
    console.log(rowdata['siteId']);
    console.log(rowdata['siteName']);
    
    this.heading = "Transport  Details for Site : "+rowdata['siteName'];
     this.showsupply = false;
     this.showtransport= true;
     this.showmisc =false;
     this.showmachine =false;

     this.dataService.getSiteTransportDetails(rowdata['siteId']).subscribe(data => {  
      this.transportData = data  ;

       // console.log(this.supplyData);
  })  

  }

  createHyperLink2(params): any {
    
    if (!params.data) { return; }
    const spanElement = document.createElement('span');
    spanElement.innerHTML = `<a href="#siteModal" data-toggle="modal" data-target="#siteModal"> ${params.value} </a> `;
    spanElement.addEventListener('click', ($event) => {
      $event.preventDefault();
      this.miscdetails(params.data);
    });
    return spanElement;
  }

  miscdetails(rowdata){
    console.log(rowdata);
    console.log(rowdata['siteId']);
    
    this.heading = "Misc Details for Site : "+rowdata['siteName'];
     this.showsupply = false;
     this.showtransport= false;
     this.showmisc =true;
     this.showmachine =false;

     this.dataService.getSiteMiscDetails(rowdata['siteId']).subscribe(data => {  
      this.miscData = data  ;

       // console.log(this.supplyData);
  }) 

  }

  createHyperLink3(params): any {
    
    if (!params.data) { return; }
    const spanElement = document.createElement('span');
    spanElement.innerHTML = `<a href="#siteModal" data-toggle="modal" data-target="#siteModal"> ${params.value} </a> `;
    spanElement.addEventListener('click', ($event) => {
      $event.preventDefault();
      this.machineydetails(params.data);
    });
    return spanElement;
  }

  machineydetails(rowdata){
    console.log(rowdata);
    console.log(rowdata['siteId']);
    
    this.heading = "Machinery Details for Site : "+rowdata['siteName'];
     this.showsupply = false;
     this.showtransport= false;
     this.showmisc =false;
     this.showmachine =true;

     this.dataService.getSiteMachineDetails(rowdata['siteId']).subscribe(data => {  
      this.machinaryData = data  ;

        console.log(this.machinaryData);
  }) 

  }

//////////
onDeleteButtonClick1(params) {
    
  console.log(params.data['id']);
    var flag = confirm("Are you sure you want to delete?");
    var obj = {};
    obj["id"] = params.data['id'] ;
    if(flag){
    this.dataService.deleteMisc(obj).subscribe(data => {  
      this.del1 = true  ;
      this.apiM.updateRowData({remove: [params.data]});
      window.location.reload();
  })
 
}
}

onDeleteButtonClicksave(params) {
    
  console.log(params.data['id']);
  console.log(params.data['expensefor']);
  console.log(params.data['amount']);
  var flag = confirm("Are you sure you want to edit?");
  var obj = {};
  obj["id"] = params.data['id'] ;
  obj["expensefor"] = params.data['expensefor'] ;
  obj["amount"] = params.data['amount'] ;

  this.dataService.updateMiscExpense(obj).subscribe(res => {
    console.log(" action  ts addMiscExpense : "+ res);
    window.location.reload();
    
  })

}


onDeleteButtonClick2(params) {
    
  console.log(params.data['id']);
    var flag = confirm("Are you sure you want to delete?");
    var obj = {};
    obj["id"] = params.data['id'] ;
    if(flag){
    this.dataService.deleteMachinery(obj).subscribe(data => {  
      this.del1 = true  ;
      this.apiMc.updateRowData({remove: [params.data]});
      window.location.reload();
  })
 
}
}

onBtnClick1(params) {
    
  console.log(params.data['Invoicenumber']);
  var flag = confirm("Are you sure you want to delete?");

  var obj = {};
  obj["invoice"]=params.data['Invoicenumber'];
  obj["tdate"]=params.data['transactiondate'];
if(flag){
  this.dataService.detailsToDelete(obj).subscribe(data => {  
   this.success1 = data  ;

   if(this.success1['status'] == 'success'){
    this.del1 = true  ;
    this.apiMc.updateRowData({remove: [params.data]});
    window.location.reload();

   }else{
     alert("Data Could not be deleted , please contact ADMIN");
   }
  })
}

}

onBtnClick2(params) {
    
  console.log(params.data['Invoicenumber']);
  var flag = confirm("Are you sure you want to delete?");

  var obj = {};
  obj["invoice"]=params.data['Invoicenumber'];
  obj["tdate"]=params.data['transactiondate'];
if(flag){
  this.dataService.detailsToDelete(obj).subscribe(data => {  
   this.success1 = data  ;
    
   if(this.success1['status'] == 'success'){
    this.del1 = true  ;
    this.apiMc.updateRowData({remove: [params.data]});
    window.location.reload();

   }else{
     alert("Data Could not be deleted , please contact ADMIN");
   }
  })
}

}

onCellValueChanged(params) {
  const colId = params.column.getId();
  console.log("onCellValueChanged " + params.column.getId())
  console.log("onCellValueChanged " + params.oldValue)
  console.log("onCellValueChanged " + params.newValue)
  if (colId === "quantity") {
  var flag =confirm("Ar you sure you want to edit Qunatity");
  if(flag == true){
    console.log("if");
  }else{
    params.newValue = params.oldValue;
    console.log("else");
    params.api.refreshCells();
  }
    //this.data.sendActToBia(params.data.activite);
}
}

}
