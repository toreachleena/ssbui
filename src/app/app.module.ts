import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AgGridModule } from 'ag-grid-angular';
import { ContactCreateComponent } from './supplyform/supplyform.component';
import { FinanceComponent } from './finance/finance.component';
import { ActionTabsComponent } from './action-tabs/action-tabs.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { ReportComponent } from './report/report.component';
import { MiscDataEntryComponent } from './misc-data-entry/misc-data-entry.component';
import { ChartsModule } from '../../node_modules/ng2-charts';
import { AccountsGridRptComponent } from './accounts-grid-rpt/accounts-grid-rpt.component';
import { TransporterGridRptComponent } from './transporter-grid-rpt/transporter-grid-rpt.component';
import { AgencyGridRptComponent } from './agency-grid-rpt/agency-grid-rpt.component';
import { MaterialsChartRptComponent } from './materials-chart-rpt/materials-chart-rpt.component';
import { SiteChartRptComponent } from './site-chart-rpt/site-chart-rpt.component';
import { SiteGridRptComponent } from './site-grid-rpt/site-grid-rpt.component';

import { DelButtonRendererComponent } from './renderer/del-button-renderer-component';
import { NumericEditorComponent } from './renderer/numeric-editor.component';
import { NumberFormatterComponent } from './renderer/NumberFormatterComponent';

import { ButtonRendererComponent } from './renderer/button-renderer.component';
import { CheckboxRenderer } from './renderer/checkbox-renderer.component';

import { FuelpurchaseComponent } from './fuelpurchase/fuelpurchase.component';
import { FueldistComponent} from './fueldist/fueldist.component';
import { LoginComponent } from './login/login.component';
import { MachineryComponent } from './machinery/machinery.component';
import { FuelreportComponent } from './fuelreport/fuelreport.component';
import { MachineryreportComponent } from './machineryreport/machineryreport.component';
import { FinancereportComponent } from './financereport/financereport.component';

//import { StoreModule } from '@ngrx/store';
///import { reducers, metaReducers } from './reducers';
//import { StoreDevtoolsModule } from '@ngrx/store-devtools';
//import { environment } from '../environments/environment';



@NgModule({
  declarations: [
    AppComponent,
    ContactCreateComponent,
    FinanceComponent,
    ActionTabsComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    ReportComponent,
    AccountsGridRptComponent,
    TransporterGridRptComponent,
    AgencyGridRptComponent,
    MaterialsChartRptComponent,
    SiteChartRptComponent,
    SiteGridRptComponent,
    MiscDataEntryComponent,
    ButtonRendererComponent,
    DelButtonRendererComponent,
    CheckboxRenderer,
    NumericEditorComponent,
    NumberFormatterComponent,
    FuelpurchaseComponent,
    LoginComponent,
    FueldistComponent,
    MachineryComponent,
    FuelreportComponent,
    MachineryreportComponent,
    FinancereportComponent,
    
  ],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ChartsModule,
    HttpClientModule,
   
    AgGridModule.withComponents([]),
    //StoreModule.forRoot(reducers, { metaReducers }),
    //!environment.production ? StoreDevtoolsModule.instrument() : []
    //AgGridModule.withComponents([ButtonRendererComponent])
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents:[ButtonRendererComponent,DelButtonRendererComponent,NumericEditorComponent,NumberFormatterComponent,CheckboxRenderer],
})
export class AppModule { }
