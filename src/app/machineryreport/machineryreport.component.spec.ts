import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineryreportComponent } from './machineryreport.component';

describe('MachineryreportComponent', () => {
  let component: MachineryreportComponent;
  let fixture: ComponentFixture<MachineryreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MachineryreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineryreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
