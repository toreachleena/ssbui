import { Component, OnInit, ViewChild } from '@angular/core';

import {  
  ColDef,  
  GridApi,  
  ColumnApi  
} from 'ag-grid-community'; 
import { AgGridAngular } from "ag-grid-angular";
import { DataService } from '../data.service';
import { DelButtonRendererComponent } from '../renderer/del-button-renderer-component';



@Component({
  selector: 'app-machineryreport',
  templateUrl: './machineryreport.component.html',
  styleUrls: ['./machineryreport.component.css']
})
export class MachineryreportComponent implements OnInit {

  title = 'Machinery Report';

  
  
  // row data and column definitions  
  public machinerydata: [];  
  public columnDefs: ColDef[];  
  // gridApi and columnApi  
  private gapi: GridApi;  
  private columnApi: ColumnApi;  
  frameworkComponents;
  show = false;
  del = false;
  rowDataClicked1={};
  machine: any [];
  heading = "";
  @ViewChild('agGrid') agGrid: AgGridAngular;
  constructor(private dataService: DataService) {  
    this.columnDefs = this.createColumnDefs();  

    this.frameworkComponents = {
      buttonRenderer: DelButtonRendererComponent,
    }
} 

onGridReady(params): void {  
  this.gapi = params.api;  
  this.columnApi = params.columnApi;  
  this.gapi.sizeColumnsToFit();  
} 
/*
 {
        "vehicletype": 5,
        "accountholdername": "Aishwarya Stone Crusher",
        "miscamount": 100,
        "trandate": "2021-07-08",
        "vehicle_type": "JCB",
        "vehiclenum": "KA340988",
        "agencyid": 44,
        "duration": "days",
        "qty": 4,
        "Total": 2350,
        "sitename": "Auto Nagar Site 1",
        "siteid": 10,
        "accountholder": 43,
        "id": 1,
        "operatorbata": 250,
        "agencyname": "Amma Iron And Steel",
        "unitrate": 500
    },
*/
// create column definitions  
private createColumnDefs() {  
  return [ 
    {  
        headerName: 'Date',  
        field: 'trandate',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'Site',  
        field: 'sitename',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'AccountHolder',  
        field: 'accountholdername',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'Agency',  
        field: 'agencyname',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'Vehicle#',  
        field: 'vehiclenum',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'Vehicle',  
        field: 'vehicle_type',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      
      {  
        headerName: 'Duration',  
        field: 'duration',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'Qty',  
        field: 'qty',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'UnitRate',  
        field: 'unitrate',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'OperatorBata',  
        field: 'operatorbata',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'MiscExpense',  
        field: 'miscdetails',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'Misc',  
        field: 'miscamount',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'Total',  
        field: 'Total',  
        filter: true,  
        editable: true,  
        resizable:true,
        sortable: true  
      },
      {  
        headerName: 'Delete',
           resizable:true,
             cellRenderer: 'buttonRenderer',
             cellRendererParams: {
              onClick: this.onDeleteButtonClick.bind(this),
              label: 'Delete'
              },
          },

    ]
  }

  ngOnInit() {
    this.dataService.getMacineryReport().subscribe(data => {  
      this.machinerydata = data  ;
  })
  }

  //////////
  onDeleteButtonClick(params) {
    
    console.log(params.data['id']);
    var flag = confirm("Are you sure you want to delete?");
    if(flag){
    this.dataService.deleteMachinery(params.data['id']).subscribe(data => {  
      this.del = true  ;
      this.gapi.updateRowData({remove: [params.data]});
  })
}

    
  
  // var selectedData = this.agGrid.api.getSelectedRows();
	//this.agGrid.api.updateRowData({ remove: selectedData });
    
  }

}
