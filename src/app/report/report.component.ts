import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {
  
  //activeTab:any  ="";
  activeTab  = "tab1";
  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public summaryData: []; 
  public barChartLabels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData = [
    {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'},
    {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'},
    {data: [10, 35, 20, 9, 36, 17, 50], label: 'Series C'}

  ];


  

  constructor(private dataService: DataService) { }
  
  ngOnInit() {
  }


  search(activeTab){

    this.activeTab = activeTab;
   // console.log("In search " + activeTab);
    if(activeTab == 'tab1'){

  
    }else if (activeTab == 'tab2'){
      
    
      //alert("Summary tab");
  
    }else if (activeTab == 'tab3'){
  
      
       
      
    }else if (activeTab == 'tab4'){
  
      
      
    }else if (activeTab == 'tab5'){
  
      
    }else if (activeTab == 'tab6'){
  
      
      
   
      
    }else if (activeTab == 'tab7'){
     
      
      
    }else if (activeTab == 'tab8'){
      
    }
  }

}