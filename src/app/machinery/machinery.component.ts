import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-machinery',
  templateUrl: './machinery.component.html',
  styleUrls: ['./machinery.component.css']
})
export class MachineryComponent implements OnInit {

  siteNames = [];
  accountHolders = [];
  agencyList = [];
  vehicles = [];

  transactiondate: any = "";
  sitenameSelected: any = "";
  agencyMSelected:any ="";
  accountHolderSelected:any="";
  vehicleSelected:any="";
  vehicleNumSelected:any="";
  quantityGiven:any="";
  durationSelected:any="";
  res:any="";
  showMsg=false;
  rate:any="";
  batarate=0;
  miscrate=0;
  total:any="";
  details:any="";

  constructor(public dataService: DataService) { }

  ngOnInit() {

    this.showMsg=false;
    this.getSitenames();
    this.getAccountHolderList();
    this.getAgencyList();
    this.getVehicleType();
  }

  getSitenames() {
    this.dataService.getSitenames().subscribe(res => {
      this.siteNames = res;
      //console.log("supplyform.ts siteNames : "+ JSON.stringify(this.siteNames));
    })
  }
  getAccountHolderList() {
    this.dataService.getAccountHolderList().subscribe(res => {
      this.accountHolders = res;
      //console.log("supplyform.ts getAccountHolderList : "+ JSON.stringify(this.accountHolders));
    })
  }

  getAgencyList() {
    this.dataService.getAgencies().subscribe(res => {
      this.agencyList = res;
     // console.log("supplyform.ts : "+ JSON.stringify(this.agencyList));
    })
  }

  getVehicleType() {
    this.dataService.getVehicleType().subscribe(res => {
      this.vehicles = res;
      //console.log("supplyform.ts vehicles : "+ JSON.stringify(this.vehicles));
    })
  }

  onSubmit(){
    console.log("Your Form Submitted *** ");
     var flag = true;
    if(this.transactiondate == ''){
      flag = false;
      alert("Please enter Transaction date");
      return false;
    }
    
    var GivenDate = this.transactiondate;
    var CurrentDate = new Date();
    GivenDate = new Date(GivenDate);

    if(GivenDate > CurrentDate){
      flag = false;
      alert('Transaction data cannot be greater than today date');
      return false;
  }
  if(this.sitenameSelected == ''){
    flag = false;
    alert("Please select the Site");
    return false;
  }

  console.log(this.agencyMSelected)
  if(this.agencyMSelected == ''){
    flag = false;
    alert("Please select AgencyName");
    return false;
  }

  if(this.accountHolderSelected == ''){
    flag = false;
    alert("Please select the AccountHolder");
    return false;
  }

  if(this.vehicleSelected == ''){
    flag = false;
    alert("Please select type of Macinery / Vehicle");
    return false;
  }

  if(this.vehicleNumSelected == ''){
    flag = false;
    alert("Please provide Vehicle Number");
    return false;
  }
  if(this.quantityGiven == ''){
    flag = false;
    alert("Please enter the Quantity");
    return false;
  }

  if(this.durationSelected == ''){
    flag = false;
    alert("Please select Duration");
    return false;
  }
  
  if(this.rate == ''){
    flag = false;
    alert("Please enter the Rate");
    return false;
  }
if( this.miscrate != 0) {
  if(this.details == ''){
    alert("Please enter the Details");
    return false;
  }
}

  var object = {};
  object["trandate"] =  this.transactiondate;
  object["siteid"] = this.sitenameSelected;
  object["agency"]=this.agencyMSelected;
  object["accholderid"]= this.accountHolderSelected;
  object["vehiclenumber"]= this.vehicleNumSelected;    
  object["vehicletypeid"]= this.vehicleSelected;
  object["qty"]= this.quantityGiven;
  object["durationSelected"]= this.durationSelected;
  object["rate"]= this.rate;
  object["bata"]= this.batarate;
  object["misc"]= this.miscrate;
  object["miscdetails"]= this.details;
  

  this.dataService.saveMachineryData(object).subscribe(res => {
    this.res = res;
    this.showMsg= true;
    console.log("saveMachineryData : "+ this.showMsg);
  })
  

  }

  calculateTotal(unitrateval){
    var qty   = this.quantityGiven;
    this.rate=unitrateval;
    this.total = qty*unitrateval;
  }
  calculateTotal1(batarate){
    var qty   = this.quantityGiven;
    var rate1 = this.rate;
    this.batarate=batarate; 
    this.total = (qty*rate1)+batarate;
  }
  calculateTotal2(miscrate){
    var qty   = this.quantityGiven;
    var rate1 = this.rate;
    var bata = this.batarate
    this.miscrate=miscrate; 
    this.total = (qty*rate1)+bata+miscrate;
  }

}
