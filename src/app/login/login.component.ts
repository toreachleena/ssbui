import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, SelectMultipleControlValueAccessor, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../data.service';
import { Routes, RouterModule } from '@angular/router';
//import { AlertService, AuthenticationService } from '../_services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  failure = false;

  username : any =""
  password : any =""
  res : any = "";
  showMsg : any ="";
 

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    public dataService: DataService,
    
    private router: Router,
    //private authenticationService: AuthenticationService,
    //private alertService: AlertService
    ) { }

  ngOnInit() {
    localStorage.removeItem('username');
  }

  loginCheck(){
    var object = {};
    //console.log(encodeURIComponent(this.password));
    object["username"] =  this.username;
    object["password"] = encodeURIComponent(this.password);
    this.dataService.loginCheck(object).subscribe(res => {
      this.res = res;
     // console.log("****** " +JSON.stringify(this.res));
      if(this.res.status == 'success'){
        localStorage.setItem('username', this.res.username);
      
        this.router.navigate(['/home']);
        setTimeout(() => 
    {
      location.reload();
  //  this.router.navigate(['/home']);
    },
2000);  
        
        //this.router.navigate(['/header']);
      }else{
        this.failure = true;
      }
      
      })
  }

}
