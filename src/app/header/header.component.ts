import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {


  username : any = "";
  showlinks : any = true;

  constructor() { }

  ngOnInit() {
    this.showlinks  = true;
    this.username = localStorage.getItem('username');
    console.log("header : " +localStorage.getItem('username'));
    if(this.username != null){
      this.showlinks = true;
    }
      console.log("header  showlinks : " +localStorage.getItem('username') + " "+ this.showlinks);
  }

}
