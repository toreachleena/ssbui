import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-misc-data-entry',
  templateUrl: './misc-data-entry.component.html',
  styleUrls: ['./misc-data-entry.component.css']
})
export class MiscDataEntryComponent implements OnInit {

  constructor(public dataService: DataService) { 

    

  }

  sitelist  = [];
  transactiondate : any = "";
  sitenameSelected : any = "";
  details : any ="";
  amount : any ="";
  res : any = "";
  showMsg = false;

  ngOnInit() {
    this.showMsg=false;
    this.getSiteNameList();
  }

  addMiscExpense(){

    if(this.transactiondate == ''){
      alert("Please select TransactionDate");
      return false;
    }
    var GivenDate = this.transactiondate;
    var CurrentDate = new Date();
    GivenDate = new Date(GivenDate);

    if(GivenDate > CurrentDate){
      alert('Transaction data cannot be greater than today date');
      return false;
  }
    if(this.sitenameSelected == ''){
      alert("Please select a site");
      return false;
    }
    if(this.details == ''){
      alert("Please enter details");
      return false;
    }
    if(this.amount == ''){
      alert("Please enter Amount");
      return false;
    }

    var object = {};
    object["tranndate"] =  this.transactiondate;
    object["siteid"] =  this.sitenameSelected;
    object["details"] =  this.details;
    object["amount"] =  this.amount;

    this.dataService.addMiscExpense(object).subscribe(res => {
      this.res = res;
      this.showMsg= true;
      console.log(" action  ts addMiscExpense : "+ res);
    })
  }

  getSiteNameList() {
    this.dataService.getSiteNameList().subscribe(res => {
      this.sitelist = res;
    })
  }

}
