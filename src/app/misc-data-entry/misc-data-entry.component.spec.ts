import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiscDataEntryComponent } from './misc-data-entry.component';

describe('MiscDataEntryComponent', () => {
  let component: MiscDataEntryComponent;
  let fixture: ComponentFixture<MiscDataEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiscDataEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiscDataEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
